//
//  SmileyConstants.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/28/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SmileyConstants : NSObject

typedef struct SmileyItemSetting_ {
    CGFloat lowValue;
    CGFloat highValue;
    CGFloat modelValue;
} SmileyItemSetting;

CG_INLINE SmileyItemSetting SmileySettingMake( CGFloat lv, CGFloat hv, CGFloat mv ){
    SmileyItemSetting s;
    s.lowValue = lv;
    s.highValue = hv;
    s.modelValue = mv;
    return s;
}


extern SmileyItemSetting const SmileyItemXOffset;
extern SmileyItemSetting const SmileyItemEyesYOffset;
extern SmileyItemSetting const SmileyItemEyesSpaceBetween;
extern SmileyItemSetting const SmileyItemEyesWidth;
extern SmileyItemSetting const SmileyItemEyesHeight;
extern SmileyItemSetting const SmileyItemMouthYOffset;
extern SmileyItemSetting const SmileyItemMouthWidth;
extern SmileyItemSetting const SmileyItemMouthHeight;
extern SmileyItemSetting const SmileyItemMouthCheekWidth;
extern SmileyItemSetting const SmileyItemMouthCheekHeight;

@end
