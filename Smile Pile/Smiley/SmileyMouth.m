//
//  SmileyMouth.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/28/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "SmileyMouth.h"
#import "Smiley.h"

@interface SmileyMouth()
@property (weak, nonatomic) Smiley * smiley;
@end

@implementation SmileyMouth
{
    CGFloat xOffset;
    
    CGFloat yOffset;
    CGFloat mouthWidth;
    CGFloat mouthHeight;
    CGFloat arcRadius;
    CGPoint startPoint;
    CGPoint endPoint;
    CGFloat cheekWidth;
    CGFloat cheekHeight;
    CGFloat cheekRadius;
    
    CGPoint leftCheekDirVector;
    CGPoint leftCheekOutVector;
    CGPoint leftCheekStartPoint;
    
    CGPoint leftCheekEndPoint;
    
    CGPoint rightCheekDirVector;
    CGPoint rightCheekOutVector;
    CGPoint rightCheekStartPoint;
    
    CGPoint rightCheekEndPoint;
}



- (id)initWithSmiley: (Smiley *) smileyFace
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        [self initialize];
        
    }
    return self;
}


- (id)initWithSmiley: (Smiley *) smileyFace andXOffset: (CGFloat) givenXOffset
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        xOffset = givenXOffset;
        [self initialize];
        
    }
    return self;
}


- (id)initSpecialWithSmiley: (Smiley *) smileyFace
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        [self initializeSpecial];
        
    }
    return self;
}



-(void) initialize
{
    /*
    yOffset = [Random randomFloatBetween: 0.10 and: 0.25] * smiley.height;
    
    mouthWidth = [Random randomFloatBetween: 0.25 and: 0.6666667] * smiley.width - fabs(xOffset);
    //mouthHeight = [Random randomFloatBetween: 1.0/15 and: 1.0/3] * mouthWidth;
    mouthHeight = [Random randomFloatBetween: 0.0666667 and: 0.14285714] * smiley.height;
    
    arcRadius = mouthHeight/2 + (mouthWidth * mouthWidth)/(8 * mouthHeight);
    
    
    startPoint = CGPointMake(smiley.center.x - mouthWidth/2 + xOffset, smiley.center.y + yOffset);
    endPoint = CGPointMake(smiley.center.x + mouthWidth/2 + xOffset, smiley.center.y + yOffset);

    //What should determine max cheek width?
    cheekWidth = [Random randomFloatBetween: 0.07142857 and: 0.125] * smiley.width;// * (mouthWidth / mouthHeight / 4);
    cheekHeight = [Random randomFloatBetween: 0.166666667 and: 0.222222222] * cheekWidth;
    
    cheekRadius = cheekHeight/2 + (cheekWidth * cheekWidth)/(8 * cheekHeight);
    */
    
    
    //=======//
    yOffset = [Random randomDistributedBetween: SmileyItemMouthYOffset.lowValue
                                           and: SmileyItemMouthYOffset.highValue
                                centeredAround: SmileyItemMouthYOffset.modelValue] * self.smiley.height;
    
    mouthWidth = [Random randomDistributedBetween: SmileyItemMouthWidth.lowValue
                                              and: SmileyItemMouthWidth.highValue
                                   centeredAround: SmileyItemMouthWidth.modelValue] * self.smiley.width - fabs(xOffset);
    
    mouthHeight = [Random randomDistributedBetween: SmileyItemMouthHeight.lowValue
                                               and: SmileyItemMouthHeight.highValue
                                    centeredAround: SmileyItemMouthHeight.modelValue] * self.smiley.height;
    
    arcRadius = mouthHeight/2 + (mouthWidth * mouthWidth)/(8 * mouthHeight);
    
    
    startPoint = CGPointMake(self.smiley.center.x - mouthWidth/2 + xOffset, self.smiley.center.y + yOffset);
    endPoint = CGPointMake(self.smiley.center.x + mouthWidth/2 + xOffset, self.smiley.center.y + yOffset);
    
    //What should determine max cheek width?
    cheekWidth = [Random randomDistributedBetween: SmileyItemMouthCheekWidth.lowValue
                                              and: SmileyItemMouthCheekWidth.highValue
                                   centeredAround: SmileyItemMouthCheekWidth.modelValue] * self.smiley.width;// * (mouthWidth / mouthHeight / 4);
    
    cheekHeight = [Random randomDistributedBetween: SmileyItemMouthCheekHeight.lowValue
                                               and: SmileyItemMouthCheekHeight.highValue
                                    centeredAround: SmileyItemMouthCheekHeight.modelValue] * cheekWidth;
    
    cheekRadius = cheekHeight/2 + (cheekWidth * cheekWidth)/(8 * cheekHeight);
    //=======//
    
    CGFloat theta = asinf(mouthWidth/2 / arcRadius);
    
    
    leftCheekDirVector = CGPointMake(cosf(theta + (M_PI*3/2)), -sinf(theta + (M_PI*3/2)));
    leftCheekOutVector = CGPointMake(leftCheekDirVector.y, -leftCheekDirVector.x);
    
    
    leftCheekStartPoint = CGPointMake(endPoint.x + leftCheekDirVector.x * -cheekWidth/2 + leftCheekOutVector.x * cheekHeight,
                                      endPoint.y + leftCheekDirVector.y * -cheekWidth/2 + leftCheekOutVector.y * cheekHeight);
    leftCheekEndPoint = CGPointMake(endPoint.x + leftCheekDirVector.x * cheekWidth/2 + leftCheekOutVector.x * cheekHeight,
                                    endPoint.y + leftCheekDirVector.y * cheekWidth/2 + leftCheekOutVector.y * cheekHeight);
    
    
    rightCheekDirVector = CGPointMake(cosf((M_PI*3/2) - theta), -sinf((M_PI*3/2) - theta));
    rightCheekOutVector = CGPointMake(rightCheekDirVector.y, -rightCheekDirVector.x);
    
    rightCheekStartPoint = CGPointMake(startPoint.x + rightCheekDirVector.x * -cheekWidth/2 + rightCheekOutVector.x * -cheekHeight,
                                       startPoint.y + rightCheekDirVector.y * -cheekWidth/2 + rightCheekOutVector.y * -cheekHeight);
    rightCheekEndPoint = CGPointMake(startPoint.x + rightCheekDirVector.x * cheekWidth/2 + rightCheekOutVector.x * -cheekHeight,
                                             startPoint.y + rightCheekDirVector.y * cheekWidth/2 + rightCheekOutVector.y * -cheekHeight);
    
}



//Used to test specific values
-(void) initializeSpecial
{
    
    yOffset = SmileyItemMouthYOffset.modelValue * self.smiley.height; //[1/10, 1/4] or [0.1, 0.25]
    
    mouthWidth = SmileyItemMouthWidth.modelValue * self.smiley.width - fabs(xOffset);//[1/4, 1/1.5] or [0.25, 0.6666667]
    mouthHeight = SmileyItemMouthHeight.modelValue * self.smiley.height;//[1/15, 1/7] or [0.06666667, 0.14285714]
    
    arcRadius = mouthHeight/2 + (mouthWidth * mouthWidth)/(8 * mouthHeight);
    
    
    startPoint = CGPointMake(self.smiley.center.x - mouthWidth/2 + xOffset, self.smiley.center.y + yOffset);
    endPoint = CGPointMake(self.smiley.center.x + mouthWidth/2 + xOffset, self.smiley.center.y + yOffset);
    
    cheekWidth = SmileyItemMouthCheekWidth.modelValue * self.smiley.width;//[1/14, 1/8] or [0.07142857, 0.125]
    cheekHeight = SmileyItemMouthCheekHeight.modelValue * cheekWidth;//[1/6, 1/4.5] or [0.166666667, 0.222222222]
    
    cheekRadius = cheekHeight/2 + (cheekWidth * cheekWidth)/(8 * cheekHeight);
    
    
    CGFloat theta = asinf(mouthWidth/2 / arcRadius);
    
    
    leftCheekDirVector = CGPointMake(cosf(theta + (M_PI*3/2)), -sinf(theta + (M_PI*3/2)));
    leftCheekOutVector = CGPointMake(leftCheekDirVector.y, -leftCheekDirVector.x);
    
    
    leftCheekStartPoint = CGPointMake(endPoint.x + leftCheekDirVector.x * -cheekWidth/2 + leftCheekOutVector.x * cheekHeight,
                                      endPoint.y + leftCheekDirVector.y * -cheekWidth/2 + leftCheekOutVector.y * cheekHeight);
    leftCheekEndPoint = CGPointMake(endPoint.x + leftCheekDirVector.x * cheekWidth/2 + leftCheekOutVector.x * cheekHeight,
                                    endPoint.y + leftCheekDirVector.y * cheekWidth/2 + leftCheekOutVector.y * cheekHeight);
    
    
    rightCheekDirVector = CGPointMake(cosf((M_PI*3/2) - theta), -sinf((M_PI*3/2) - theta));
    rightCheekOutVector = CGPointMake(rightCheekDirVector.y, -rightCheekDirVector.x);
    
    rightCheekStartPoint = CGPointMake(startPoint.x + rightCheekDirVector.x * -cheekWidth/2 + rightCheekOutVector.x * -cheekHeight,
                                       startPoint.y + rightCheekDirVector.y * -cheekWidth/2 + rightCheekOutVector.y * -cheekHeight);
    rightCheekEndPoint = CGPointMake(startPoint.x + rightCheekDirVector.x * cheekWidth/2 + rightCheekOutVector.x * -cheekHeight,
                                     startPoint.y + rightCheekDirVector.y * cheekWidth/2 + rightCheekOutVector.y * -cheekHeight);
}



-(void) drawMouth:(CGContextRef)context
{
    
    [Draw drawArcOnContext: context
                   OfWidth: self.smiley.size * (0.7/25)
                   OfColor: self.smiley.outlineColor.CGColor
            WithStartPoint: startPoint
               AndEndPoint: endPoint
                 AndRadius: arcRadius
              WithArcAbove: NO];
    
    
    const float drawCheekProbability = 0.99f;
    
    //Draw left cheek
    if(rand()/(float)RAND_MAX < drawCheekProbability)
    {
        [Draw drawArcOnContext: context
                       OfWidth: self.smiley.size * (0.5/25)
                       OfColor: self.smiley.outlineColor.CGColor
                WithStartPoint: leftCheekStartPoint
                   AndEndPoint: leftCheekEndPoint
                     AndRadius: cheekRadius
                  WithArcAbove: NO];
        
        //        [Draw drawLineOnContext:context from:leftCheekStartPoint to:leftCheekEndPoint ofSize:self.size * (1.0/25) ofColor:self.smiley.outlineColor.CGColor];
        
    }
    
    //Draw right cheek
    if(rand()/(float)RAND_MAX < drawCheekProbability)
    {
        [Draw drawArcOnContext: context
                       OfWidth: self.smiley.size * (0.5/25)
                       OfColor: self.smiley.outlineColor.CGColor
                WithStartPoint: rightCheekStartPoint
                   AndEndPoint: rightCheekEndPoint
                     AndRadius: cheekRadius
                  WithArcAbove: YES];
        
        //        [Draw drawLineOnContext:context from:rightCheekStartPoint to:rightCheekEndPoint ofSize:self.size * (1.0/25) ofColor:self.smiley.outlineColor.CGColor];
    }
    
    
}

@end
