//
//  SmileyEyes.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/28/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "SmileyEyes.h"
#import "Smiley.h"

@interface SmileyEyes()
@property (weak, nonatomic) Smiley * smiley;
@end


@implementation SmileyEyes
{
    //Smiley * smiley;
    
    CGFloat spaceBetween;
    CGFloat xOffset;
    CGFloat yOffset;
    CGFloat eyeWidth;
    CGFloat eyeHeight;
    CGFloat eyeCapHeight;
    CGFloat eyeLineHeight;
    CGPoint topOfLeftEyeLine;
    CGPoint bottomOfLeftEyeLine;
    CGRect upperLeftEyeCapBounds;
    CGRect lowerLeftEyeCapBounds;
    CGPoint topOfRightEyeLine;
    CGPoint BottomOfRightEyeLine;
    CGRect upperRightEyeCapBounds;
    CGRect lowerRightEyeCapBounds;
}

- (id)initWithSmiley: (Smiley *) smileyFace
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        [self initialize];
        
    }
    return self;
}

- (id)initWithSmiley: (Smiley *) smileyFace andXOffset: (CGFloat) givenXOffset
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        xOffset = givenXOffset;
        [self initialize];
        
    }
    return self;
}

- (id)initSpecialWithSmiley: (Smiley *) smileyFace
{
    self = [super init];
    if (self) {
        
        self.smiley = smileyFace;
        [self initializeSpecial];
        
    }
    return self;
}


-(void) initialize
{
    /*
    spaceBetween = [Random randomFloatBetween: 0.125 and: 0.25] * smiley.width;
    yOffset = [Random randomFloatBetween: 0.125 and: 0.20] * smiley.height;
    
    eyeWidth = [Random randomFloatBetween: 0.05 and: 0.06666667] * smiley.width;
    eyeHeight = [Random randomFloatBetween: 0.06666667 and: 0.25] * smiley.height;
    
    eyeCapHeight = [Random randomFloatBetween: 0 and: eyeHeight];
    eyeLineHeight = eyeHeight - eyeCapHeight;
    */
    
    
    spaceBetween = [Random randomDistributedBetween: SmileyItemEyesSpaceBetween.lowValue
                                                and: SmileyItemEyesSpaceBetween.highValue
                                     centeredAround: SmileyItemEyesSpaceBetween.modelValue] * self.smiley.width;
    
    yOffset = [Random randomDistributedBetween: SmileyItemEyesYOffset.lowValue
                                           and: SmileyItemEyesYOffset.highValue
                                centeredAround: SmileyItemEyesYOffset.modelValue] * self.smiley.height;
    
    eyeWidth = [Random randomDistributedBetween: SmileyItemEyesWidth.lowValue
                                            and: SmileyItemEyesWidth.highValue
                                 centeredAround: SmileyItemEyesWidth.modelValue] * self.smiley.width;
    
    eyeHeight = [Random randomDistributedBetween: SmileyItemEyesHeight.lowValue
                                             and: SmileyItemEyesHeight.highValue
                                  centeredAround: SmileyItemEyesHeight.modelValue] * self.smiley.height;
    
    eyeCapHeight = [Random randomDistributedBetween: 0 and: eyeHeight centeredAround:eyeWidth];
    eyeLineHeight = eyeHeight - eyeCapHeight;
    
    
    
    topOfLeftEyeLine = CGPointMake(self.smiley.center.x - spaceBetween/2 + xOffset, self.smiley.center.y - eyeLineHeight/2 - yOffset);
    bottomOfLeftEyeLine = CGPointMake(self.smiley.center.x - spaceBetween/2 + xOffset, self.smiley.center.y + eyeLineHeight/2 - yOffset);
    
    upperLeftEyeCapBounds = CGRectMake(topOfLeftEyeLine.x - eyeWidth/2, topOfLeftEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);
    lowerLeftEyeCapBounds = CGRectMake(bottomOfLeftEyeLine.x - eyeWidth/2, bottomOfLeftEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);;
    

    topOfRightEyeLine = CGPointMake(self.smiley.center.x + spaceBetween/2 + xOffset, self.smiley.center.y - eyeLineHeight/2 - yOffset);
    BottomOfRightEyeLine = CGPointMake(self.smiley.center.x + spaceBetween/2 + xOffset, self.smiley.center.y + eyeLineHeight/2 - yOffset);
    
    upperRightEyeCapBounds = CGRectMake(topOfRightEyeLine.x - eyeWidth/2, topOfRightEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);
    lowerRightEyeCapBounds = CGRectMake(BottomOfRightEyeLine.x - eyeWidth/2, BottomOfRightEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);;
    
}



//Used to test specific values
-(void) initializeSpecial
{
    spaceBetween = SmileyItemEyesSpaceBetween.modelValue * self.smiley.width;     //[1/8, 1/4] or [0.125, 0.25]
    yOffset = SmileyItemEyesYOffset.modelValue * self.smiley.height;         //[1/8, 1/5] or [0.125, 0.20]
    
    eyeWidth = SmileyItemEyesWidth.modelValue * self.smiley.width;         //[1/20, 1/15] or [0.05, 0.06666667]
    eyeHeight = SmileyItemEyesHeight.modelValue  * self.smiley.height;      //[1/15, 1/4] or [0.06666667, 0.25]
    
    eyeCapHeight = eyeWidth;                //[0, eyeHeight] or [0.0, eyeHeight]
    eyeLineHeight = eyeHeight - eyeCapHeight;
    
    
    topOfLeftEyeLine = CGPointMake(self.smiley.center.x - spaceBetween/2 + xOffset, self.smiley.center.y - eyeLineHeight/2 - yOffset);
    bottomOfLeftEyeLine = CGPointMake(self.smiley.center.x - spaceBetween/2 + xOffset, self.smiley.center.y + eyeLineHeight/2 - yOffset);
    
    upperLeftEyeCapBounds = CGRectMake(topOfLeftEyeLine.x - eyeWidth/2, topOfLeftEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);
    lowerLeftEyeCapBounds = CGRectMake(bottomOfLeftEyeLine.x - eyeWidth/2, bottomOfLeftEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);;
    
    
    topOfRightEyeLine = CGPointMake(self.smiley.center.x + spaceBetween/2 + xOffset, self.smiley.center.y - eyeLineHeight/2 - yOffset);
    BottomOfRightEyeLine = CGPointMake(self.smiley.center.x + spaceBetween/2 + xOffset, self.smiley.center.y + eyeLineHeight/2 - yOffset);
    
    upperRightEyeCapBounds = CGRectMake(topOfRightEyeLine.x - eyeWidth/2, topOfRightEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);
    lowerRightEyeCapBounds = CGRectMake(BottomOfRightEyeLine.x - eyeWidth/2, BottomOfRightEyeLine.y - eyeCapHeight/2, eyeWidth, eyeCapHeight);;
    
    
    
}

-(void) drawEyes: (CGContextRef) context
{
    //
    
    //Left Eye
    
    //Draw upper left eye cap
    [Draw drawEllipseOnContext: context InRect: upperLeftEyeCapBounds OfColor: self.smiley.outlineColor.CGColor];
    
    //Draw inner left eye line
    [Draw drawLineOnContext: context
                       from: topOfLeftEyeLine
                         to: bottomOfLeftEyeLine
                     ofSize: eyeWidth
                    ofColor: self.smiley.outlineColor.CGColor];
    
    //Draw lower left eye cap
    [Draw drawEllipseOnContext: context InRect: lowerLeftEyeCapBounds OfColor: self.smiley.outlineColor.CGColor];
    
    
    
    //Right Eye
    
    //Draw upper left eye cap
    [Draw drawEllipseOnContext: context InRect: upperRightEyeCapBounds OfColor: self.smiley.outlineColor.CGColor];
    
    //Draw inner right eye line
    [Draw drawLineOnContext: context
                       from: topOfRightEyeLine
                         to: BottomOfRightEyeLine
                     ofSize: eyeWidth
                    ofColor: self.smiley.outlineColor.CGColor];
    
    //Draw lower left eye cap
    [Draw drawEllipseOnContext: context InRect: lowerRightEyeCapBounds OfColor: self.smiley.outlineColor.CGColor];
}

@end
