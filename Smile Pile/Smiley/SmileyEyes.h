//
//  SmileyEyes.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/28/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Draw.h"
#import "Random.h"

@class Smiley;


@interface SmileyEyes : NSObject

-(id) initWithSmiley: (Smiley *) smileyFace;
- (id)initWithSmiley: (Smiley *) smileyFace andXOffset: (CGFloat) givenXOffset;
- (id)initSpecialWithSmiley: (Smiley *) smileyFace;
-(void) drawEyes: (CGContextRef) context;

@end
