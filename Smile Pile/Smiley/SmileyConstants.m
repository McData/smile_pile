//
//  SmileyConstants.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/28/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SmileyConstants.h"

@implementation SmileyConstants

//LowValue, HighValue, ModelValue

SmileyItemSetting const SmileyItemXOffset           = {-1, 1, 0};

SmileyItemSetting const SmileyItemEyesYOffset       = {0.1, 0.3, 0.2};
SmileyItemSetting const SmileyItemEyesSpaceBetween  = {0.125, 0.20, 0.15};
SmileyItemSetting const SmileyItemEyesWidth         = {0.02, 0.07, 0.045};
SmileyItemSetting const SmileyItemEyesHeight        = {0.06, 0.35, 0.2};

SmileyItemSetting const SmileyItemMouthYOffset      = {0.10, 0.25, 0.17};
SmileyItemSetting const SmileyItemMouthWidth        = {0.25, 0.7, 0.45};
SmileyItemSetting const SmileyItemMouthHeight       = {0.05, 0.15, 0.10};
SmileyItemSetting const SmileyItemMouthCheekWidth   = {0.07, 0.125, 0.1};
SmileyItemSetting const SmileyItemMouthCheekHeight  = {0.16, 0.23, 0.2};

@end
