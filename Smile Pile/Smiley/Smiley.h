//
//  Smiley.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingsData.h"
#import "Draw.h"
#import "Random.h"
#import "SmileyEyes.h"
#import "SmileyMouth.h"
#include <math.h>
#import "SmileyConstants.h"

@interface Smiley : NSObject

@property CGPoint center;
@property CGFloat size;

@property float jelloAmount;

@property CGFloat width;
@property CGFloat height;

@property CGRect elipseBounds;
@property CGRect innerElipseBounds;

@property UIColor * smileyColor;
@property UIColor * outlineColor;

@property SmileyEyes * eyes;
@property SmileyMouth * mouth;

- (id)initWithCenter: (CGPoint) center;
- (id)initWithCenter: (CGPoint) center andSize: (CGFloat) size;
-(id) initSpecialWithCenter: (CGPoint) center;

-(UIImage *) draw: (UIImage *) image;

@end



