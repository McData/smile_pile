//
//  Smiley.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "Smiley.h"
#import "ColorSettings.h"

@implementation Smiley


- (id)initWithCenter: (CGPoint) center
{
    self = [super init];
    if (self) {
        self.center = center;
        self.size = [((NSNumber*)[SettingsData getObjectFromIndex: kSmileySize]) floatValue];
        
        [self initialize];
    }
    return self;
}

- (id)initWithCenter: (CGPoint) center andSize: (CGFloat) size
{
    self = [super init];
    if (self) {
        self.center = center;
        self.size = size;
        
        [self initialize];
    }
    return self;
}

//Used to create model citizen
-(id) initSpecialWithCenter: (CGPoint) center
{
    self = [super init];
    if (self) {
        self.center = center;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            self.size = 125;
        }
        else
        {
            self.size = 225;
        }
        [self initializeSpecial];
    }
    return self;
}


-(void) initialize
{
    self.smileyColor = [((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]) getSmileyColor];
    self.outlineColor = [ColorSettings getOutlineColorForSmileyColor:self.smileyColor];
    
    
    self.jelloAmount = [Random randomDistributedBetween:1 and: [[SettingsData getObjectFromIndex:kJelloAmount] floatValue] centeredAround:1];
    
    self.width = self.size;
    self.height = self.size;
    
    if([Random randomFloatBetween:0 and:1] < 0.5f) {
        self.width /=  (self.jelloAmount);
    }
    else {
        self.height /=  (self.jelloAmount);
    }
    
    self.elipseBounds = CGRectMake(self.center.x - self.width/2, self.center.y - self.height/2, self.width, self.height);
    self.innerElipseBounds = CGRectMake(self.center.x - self.width*(24.0/25.0)/2, self.center.y - self.height*(24.0/25.0)/2, self.width*(24.0/25.0), self.height*(24.0/25.0));
    
    CGFloat xOffset = 0;
    
    if([Random randomFloatBetween:0 and:1] < 0.5f) {
        xOffset = [Random randomDistributedBetween:SmileyItemXOffset.lowValue and:SmileyItemXOffset.highValue centeredAround:SmileyItemXOffset.modelValue] * self.width * 0.2;
        
    }
    
    self.eyes = [[SmileyEyes alloc] initWithSmiley: self andXOffset:xOffset];
    self.mouth = [[SmileyMouth alloc] initWithSmiley: self andXOffset:xOffset];
}


//Used to test specific values
-(void) initializeSpecial
{
    self.smileyColor = [((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]) getColorPresetForIndex:0];
    self.outlineColor = [ColorSettings getOutlineColorForSmileyColor:self.smileyColor];
    
    self.jelloAmount = 1;
    
    self.width = self.size;
    self.height = self.size;
    
    self.elipseBounds = CGRectMake(self.center.x - self.width/2, self.center.y - self.height/2, self.width, self.height);
    self.innerElipseBounds = CGRectMake(self.center.x - self.width*(24.0/25.0)/2, self.center.y - self.height*(24.0/25.0)/2, self.width*(24.0/25.0), self.height*(24.0/25.0));
    
    self.eyes = [[SmileyEyes alloc] initSpecialWithSmiley: self];
    self.mouth = [[SmileyMouth alloc] initSpecialWithSmiley: self];
}



-(UIImage *) draw: (UIImage *) image
{
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(image.size);
    }
    
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Draw background oval
    [self drawCircle:context];
    
    //Draw Eyes
    [self.eyes drawEyes:context];
    
    //Draw Mouth
    [self.mouth drawMouth:context];

    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


-(void) drawCircle: (CGContextRef) context
{    
    
    [Draw drawEllipseOnContext: context InRect: self.elipseBounds OfColor: [UIColor blackColor].CGColor];//self.outlineColor.CGColor
    
    [Draw drawEllipseOnContext: context InRect:self.innerElipseBounds OfColor: self.smileyColor.CGColor];
}

@end
