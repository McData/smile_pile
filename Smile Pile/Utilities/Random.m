//
//  Random.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/22/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "Random.h"

@implementation Random

+(int) randomIntBetween:(int) smallNumber and:(float) bigNumber
{
    int diff = bigNumber - smallNumber;
    return ((int)round( (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

+ (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber
{
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

+(float) randomDistributedBetween:(float) lowValue and: (float) highValue centeredAround:(float) middleValue
{
    if(lowValue >= highValue)
        return middleValue;
    
    float randValue = [self randomFloatBetween:-1 and:1];
    
    float returnValue = middleValue;
    float m0 = (middleValue - lowValue)*2.0/(highValue - lowValue);
    
    //NSLog(@"LV: %f, M: %f, HV: %f, M0: %f", lowValue, middleValue, highValue, m0);
    
    
    if(randValue+1 <= m0)
    {
        float value1 = 1-((randValue - (m0-1))*(randValue - (m0-1)))/(m0*m0);
        float value2 = middleValue - lowValue;
        value2 = value2 * value2;
        
        returnValue = sqrtf(value1*value2) + lowValue;
    }
    else
    {
        float value1 = 1-((randValue - (m0-1))*(randValue - (m0-1)))/((2-m0)*(2-m0));
        float value2 = middleValue - highValue;
        value2 = value2 * value2;
        
        returnValue = -1*sqrtf(value1*value2) + highValue;
    }
    
    //NSLog(@"Random: %f, Return: %f", randValue, returnValue);
    return returnValue;
}


+(float) randomDistributionBetween:(float) lowValue and: (float) highValue centeredAround:(float) middleValue
{
    //If lower or higher value is too close to the middle value, don't consider that part of the range
    int lower = (middleValue - lowValue < .001)? 0: -1;
    int upper = (highValue - middleValue < .001)? 0: 1;
    
    float randValue = [self randomFloatBetween:lower and:upper];
    
    float returnValue = middleValue;
    
    float value1 = 1-(randValue*randValue);
    
    if(randValue <= 0)
    {
        float value2 = middleValue - lowValue;
        value2 = value2 * value2;
        
        returnValue = sqrtf(value1*value2) + lowValue;
    }
    else
    {
        float value2 = middleValue - highValue;
        value2 = value2 * value2;
        
        returnValue = -1*sqrtf(value1*value2) + highValue;
    }
    return returnValue;
}

@end
