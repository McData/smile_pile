//
//  Draw.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Draw : NSObject

+ (CGColorRef) blackColor;
+ (CGColorRef) whiteColor;


+ (UIImage *)imageWithColor:(UIColor *)color andBounds:(CGRect)imgBounds;

+(void) drawLineFrom: (CGPoint) pointFrom to: (CGPoint) pointTo ofSize: (CGFloat) lineSize ofColor: (CGColorRef) color onImage: (UIImageView *) imageView;




+(void) drawLineOnContext: (CGContextRef) context from: (CGPoint) pointFrom to: (CGPoint) pointTo ofSize: (CGFloat) lineSize ofColor: (CGColorRef) color;

+(void) drawEllipseOnContext: (CGContextRef) context InRect: (CGRect) rect OfColor: (CGColorRef) color;
+(void) drawEllipseOutlineOnContext: (CGContextRef) context InRect: (CGRect) rect OfColor: (CGColorRef) color;

+(void) drawEllipseGradientOnContext: (CGContextRef) context from: (CGPoint) startPoint to: (CGPoint) endPoint withStartBounds:(CGRect) startBounds andEndBounds: (CGRect) endBounds withStartColor: (CGColorRef) startColor andEndColor: (CGColorRef) endColor;
+(void) drawGradientLinearOnContext:(CGContextRef) context from: (CGPoint) startPoint to: (CGPoint) endPoint withStartColor: (CGColorRef) startColor andEndColor: (CGColorRef) endColor;

+(void) drawArcOnContext: (CGContextRef) context OfWidth: (CGFloat) lineWidth OfColor: (CGColorRef) color WithStartPoint: (CGPoint) startPoint AndEndPoint: (CGPoint) endPoint AndRadius: (CGFloat) radius WithArcAbove: (BOOL) above;
@end
