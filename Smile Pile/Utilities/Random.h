//
//  Random.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/22/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Random : NSObject

+ (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber;
+(float) randomDistributedBetween:(float) lowValue and: (float) highValue centeredAround:(float) middleValue;
@end
