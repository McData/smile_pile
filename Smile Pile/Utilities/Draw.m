//
//  Draw.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "Draw.h"

@implementation Draw

static CGColorRef blackColor = NULL;
static CGColorRef whiteColor = NULL;

+ (CGColorRef) blackColor
{
    if(blackColor == NULL)
    {
        CGFloat components[] = {0.0f,0.0f,0.0f,1.0f};
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        blackColor = CGColorCreate(colorSpace, components);
        
        CGColorSpaceRelease(colorSpace), colorSpace = NULL;
    }
    return blackColor;
}

+ (CGColorRef) whiteColor
{
    if(whiteColor == NULL)
    {
        CGFloat components[] = {1.0f,1.0f,1.0f,1.0f};
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        whiteColor = CGColorCreate(colorSpace, components);
        
        CGColorSpaceRelease(colorSpace), colorSpace = NULL;
    }
    return whiteColor;
}


+ (UIImage *)imageWithColor:(UIColor *)color andBounds:(CGRect)imgBounds
{
    UIGraphicsBeginImageContextWithOptions(imgBounds.size, NO, 0.0);
    [color setFill];
    UIRectFill(imgBounds);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

//Temporary
+(void) drawLineFrom: (CGPoint) pointFrom to: (CGPoint) pointTo ofSize: (CGFloat) lineSize ofColor: (CGColorRef) color onImage: (UIImageView *) imageView
{
    //UIGraphicsBeginImageContext(imageView.frame.size);
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(imageView.frame.size, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(imageView.frame.size);
    }
    
    
    [imageView.image drawInRect:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextSetLineWidth(context, lineSize);
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    
    const CGFloat * colorComponents = CGColorGetComponents(color);
    CGContextSetRGBStrokeColor(context, colorComponents[0], colorComponents[1], colorComponents[2], colorComponents[3]);
    
    CGContextMoveToPoint(context, pointFrom.x, pointFrom.y);
    CGContextAddLineToPoint(context, pointTo.x, pointTo.y);
    
    CGContextStrokePath(context);
    
    CGContextFlush(context);
    
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
   
    UIGraphicsEndImageContext();
}


+(void) drawLineOnContext: (CGContextRef) context from: (CGPoint) pointFrom to: (CGPoint) pointTo ofSize: (CGFloat) lineSize ofColor: (CGColorRef) color
{
    
    CGContextSetLineWidth(context, lineSize);
    CGContextSetStrokeColorWithColor(context, color);
    
    CGContextMoveToPoint(context, pointFrom.x, pointFrom.y);
    CGContextAddLineToPoint(context, pointTo.x, pointTo.y);
    
    CGContextStrokePath(context);
    
    //CGContextFlush(context);
}


+(void) drawEllipseOnContext: (CGContextRef) context InRect: (CGRect) rect OfColor: (CGColorRef) color
{
    CGContextSetFillColorWithColor(context, color);
    CGContextAddEllipseInRect(context, rect);
    CGContextFillPath(context);
    
    //CGContextFlush(context);
}



+(void) drawEllipseOutlineOnContext: (CGContextRef) context InRect: (CGRect) rect OfColor: (CGColorRef) color
{
    CGContextSetStrokeColorWithColor(context, color);
    CGContextAddEllipseInRect(context, rect);
    
    CGContextSetLineWidth(context, 2.0);
    CGContextStrokePath(context);
    
    //CGContextFlush(context);
}

+(void) drawEllipseGradientOnContext: (CGContextRef) context from: (CGPoint) startPoint to: (CGPoint) endPoint withStartBounds:(CGRect) startBounds andEndBounds: (CGRect) endBounds withStartColor: (CGColorRef) startColor andEndColor: (CGColorRef) endColor
{
    
    const CGFloat * startColorComponents = CGColorGetComponents(startColor);
    const CGFloat * endColorComponents = CGColorGetComponents(endColor);
    
    CGFloat colors [] = {
        startColorComponents[0], startColorComponents[1], startColorComponents[2], startColorComponents[3],
        endColorComponents[0], endColorComponents[1], endColorComponents[2], endColorComponents[3]
    };
    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
    
    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
    
    
    //CGContextSaveGState(context);
    
    CGContextAddEllipseInRect(context, endBounds);
    
    CGFloat startLargerRadius = MAX(startBounds.size.width, startBounds.size.height)/2.0;
    CGFloat endLargerRadius = MAX(endBounds.size.width, endBounds.size.height)/2.0;
    
    CGContextDrawRadialGradient(context, gradient, startPoint, startLargerRadius, endPoint, endLargerRadius, kCGGradientDrawsBeforeStartLocation);
    CGGradientRelease(gradient), gradient = NULL;
    //CGContextRestoreGState(context);
}

+(void) drawGradientLinearOnContext:(CGContextRef) context from: (CGPoint) startPoint to: (CGPoint) endPoint withStartColor: (CGColorRef) startColor andEndColor: (CGColorRef) endColor
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    const CGFloat * startColorComponents = CGColorGetComponents(startColor);
    const CGFloat * endColorComponents = CGColorGetComponents(endColor);
    
    CGFloat colors [] = {
        startColorComponents[0], startColorComponents[1], startColorComponents[2], startColorComponents[3],
        endColorComponents[0], endColorComponents[1], endColorComponents[2], endColorComponents[3]
    };

    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, NULL, 2);
    
    CGContextSaveGState(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

+(void) drawArcOnContext: (CGContextRef) context OfWidth: (CGFloat) lineWidth OfColor: (CGColorRef) color WithStartPoint: (CGPoint) startPoint AndEndPoint: (CGPoint) endPoint AndRadius: (CGFloat) radius WithArcAbove: (BOOL) above
{
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    
    
    CGPoint midPoint = CGPointMake((startPoint.x + endPoint.x)/2, (startPoint.y + endPoint.y)/2);
    
    //NSLog(@"Start: (%f %f)", startPoint.x, startPoint.y);
    //NSLog(@"End:   (%f %f)", endPoint.x, endPoint.y);
    //NSLog(@"Mid:   (%f %f)", midPoint.x, midPoint.y);
    
    CGPoint startToMidVector = CGPointMake(midPoint.x - startPoint.x, midPoint.y - startPoint.y);
    
    //NSLog(@"Start to mid Vector: %f %f", startToMidVector.x, startToMidVector.y);
    
    CGFloat a = fmax(sqrtf(startToMidVector.x * startToMidVector.x + startToMidVector.y * startToMidVector.y), 0.0000000000000001);
    CGFloat theta = asinf(fmaxf(fminf(0.999999f, a/radius),-0.999999f));
    CGFloat length = a * tanf(theta);
    
    //NSLog(@"Theta: %f", theta);
    
    //NSLog(@"a: %f theta: %f length: %f", a, theta, length);
    
    CGPoint dir = CGPointMake((above? 1: -1) * startToMidVector.y/a * length, (above? -1: 1) * startToMidVector.x/a * length);
    
    //NSLog(@"dir:   (%f %f)", dir.x, dir.y);
    
    CGPoint arcPoint = CGPointMake(midPoint.x + dir.x, midPoint.y + dir.y);
    
    //NSLog(@"arcPoint:   (%f %f)", arcPoint.x, arcPoint.y);
    
    CGContextAddArcToPoint(context, arcPoint.x, arcPoint.y, endPoint.x, endPoint.y, radius);
    CGContextStrokePath(context);
    
    //CGContextFlush(context);
}



@end
