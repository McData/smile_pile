//
//  SeparatedPileDrawType.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SeparatedPileDrawType.h"
#import "PilePartition.h"

@implementation SeparatedPileDrawType
{
    PilePartition * pilePartition;
    BOOL partitionFull;
}

- (void) initializePile
{
    @autoreleasepool {
        pilePartition = [[PilePartition alloc] initWithSize:frame.size];
        partitionFull = NO;
    }
}

- (void) finalizePile
{
    BOOL loopSetting = [((NSNumber*)[SettingsData getObjectFromIndex: kSmilePileLoopBool]) boolValue];
    if (!loopSetting && partitionFull)
    {
        pilePartition = nil;
        [self notifyOfPileFinished];
    }
}

- (void) smileyLoop: (NSDate *) timeOfGoButtonPress
{
    BOOL loopSetting = [((NSNumber*)[SettingsData getObjectFromIndex: kSmilePileLoopBool]) boolValue];
    do
    {
        for (int currSmiley = 0; active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime]; ++currSmiley)
        {
            NSDate* timeBeforeDrawingCurrentSmiley = [NSDate date];
            
            CGFloat size;
            CGPoint center = [pilePartition getSmileyLocationWithMaxSizeOf: [[SettingsData getObjectFromIndex:kSmileySize] floatValue] andSizePointer: &size ];
                
            //No space left, done drawing smilies.
            if(center.x == -1 && center.y == -1) {
                partitionFull = YES;
                break;
            }
            
            @autoreleasepool {
                Smiley * currentSmiley = [[Smiley alloc] initWithCenter: center andSize: size];
                
                //Add smiley draw call to the main thread to update UI
                [self drawSmiley: currentSmiley withTime:timeOfGoButtonPress];
                
                [pilePartition addSmiley: currentSmiley];
            }
            
            
            if(active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime])
            {
                //Sleep thread so that all smiles do not appear at once
                //Sleep based on settings amount.
                double sleepTime = [(NSNumber* ) [SettingsData getObjectFromIndex:kSmileyDrawSpeed] doubleValue] - [[NSDate date] timeIntervalSinceDate:timeBeforeDrawingCurrentSmiley] ;
                sleepTime = MAX(sleepTime, 0);
                [NSThread sleepForTimeInterval: sleepTime];
            }
        }
        
        if(active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime] && loopSetting)
        {
            double sleepTime = [(NSNumber* ) [SettingsData getObjectFromIndex:kSmileyDrawSpeed] doubleValue];
            [NSThread sleepForTimeInterval: sleepTime];
            
            [self clearSmilePile];
            [self initializePile];
        }
        
    } while (active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime] && loopSetting);
}



@end
