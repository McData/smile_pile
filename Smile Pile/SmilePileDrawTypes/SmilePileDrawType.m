//
//  SmilePileDrawType.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SmilePileDrawType.h"
#import "ColorSettings.h"

@implementation SmilePileDrawType


- (id) initWithView: (MainViewController * ) view dispatchQueue: (dispatch_queue_t) queue frame: (CGRect) frameRect
{
    self = [super init];
    if (self) {
        
        mainView = view;
        drawSmilesQueue = queue;
        frame = CGRectMake(0, 0, frameRect.size.width, frameRect.size.height);
        active = NO;
        [self clearSmilePile];
    }
    return self;
}

-(void) setActive: (BOOL) newActive
{
    active = newActive;
}

-(void) setGoButtonTime: (NSDate*) goTime
{
    goButtonPressTime = goTime;
}

-(void) startCreatingSmilies: (NSDate *) goTime
{
    goButtonPressTime = goTime;
    
    NSDate* goButtonPressTimeCopy = [[NSDate alloc] initWithTimeInterval:0 sinceDate:goButtonPressTime];
    dispatch_async(drawSmilesQueue, ^{
        [self clearSmilePile];
        active = YES;
        [self initializePile];
        [self smileyLoop: goButtonPressTimeCopy];
        [self finalizePile];
        active = NO;
    });
}

- (void) resumeCreatingSmilies
{
    NSDate* goButtonPressTimeCopy = [[NSDate alloc] initWithTimeInterval:0 sinceDate:goButtonPressTime];
    dispatch_async(drawSmilesQueue, ^{
        active = YES;
        [self smileyLoop: goButtonPressTimeCopy];
        [self finalizePile];
        active = NO;
    });
}

- (void) initializePile {}

- (void) finalizePile
{
    [self notifyOfPileFinished];
}

- (void) smileyLoop: (NSDate *) timeOfGoButtonPress
{
    [NSException raise:@"Invoked abstract method" format:@"Invoked abstract method"];
}


- (void) clearSmilePile
{
    @autoreleasepool {
        self.smilePileImage = [Draw imageWithColor:[UIColor clearColor] andBounds:frame];
    }
}

-(void) drawSmiley: (Smiley *) smiley withTime: (NSDate*) timeOfGoButtonPress
{
    if (![timeOfGoButtonPress isEqualToDate:goButtonPressTime]) {
        return;
    }
    
    //Draw Smiley onto image to display to the user
    self.smilePileImage = [smiley draw: self.smilePileImage];
    
    //Update displayed image
    dispatch_async(dispatch_get_main_queue(), ^{
        [mainView updateDisplayedImage:self.smilePileImage];
    });
}

-(void) notifyOfPileFinished
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [mainView pileFinished];
    });
}

@end
