//
//  OverlappingPileDrawType.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SmilePileDrawType.h"

@interface OverlappingPileDrawType : SmilePileDrawType

@end
