//
//  PilePartition.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/29/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "PilePartition.h"

@implementation PilePartition
{
    int** partitionData;       //Two dimensional array of integers represeting the screen
    NSArray* locations;        //Array of mutable index sets. Used to hold pixel locations for each possible smiley size
    
    int maxLocationIndex;
    
    NSInteger minSmileySize;
    
    CGFloat scale;
    int maxValue;
}

static const int MAX_DATA_VALUE = 150; //(Max size should be half the max smiley size)


- (id)initWithSize:(CGSize) size
{
    self = [super init];
    if (self) {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            scale = (2.0/5.0);
        }
        else
        {
            scale = 1.0;
        }
        
        self.size = CGSizeMake(size.width * scale, size.height * scale);
        //size.height *= scale;
        //size.width *= scale;
        maxValue = (int)(MAX_DATA_VALUE * scale);
        minSmileySize = [[SettingsData getObjectFromIndex: kMinSmileySize] integerValue] * scale;
        
        //self.size = size;
        
        
        partitionData = (int**)malloc(self.size.width * sizeof(int*));
        for(int x = 0; x < self.size.width; ++x)
        {
            partitionData[x] = (int*)malloc(self.size.height * sizeof(int));
            for(int y = 0; y < self.size.height; ++y)
            {
                partitionData[x][y] = 0;
            }
        }
        
        
        NSMutableArray * initialArray = [NSMutableArray arrayWithCapacity:maxValue];
        for (int currSize = 0; currSize <= maxValue; ++currSize)
        {
            initialArray[currSize] = [[NSMutableIndexSet alloc] init];
        }
        locations = [NSArray arrayWithArray:initialArray];
        
        maxLocationIndex = maxValue;
        
        [self initialize];
        
    }
    return self;
}

- (void)dealloc
{
    for(int curr = 0; curr < self.size.width; ++curr)
    {
        free(partitionData[curr]);
    }
    free(partitionData);
}

-(void)reset
{
    [self initialize];
}


-(int) indexOfPixelAtX: (int) x AndY: (int) y
{
    return y * self.size.width + x;
}

-(CGPoint) pixelFromIndex: (NSUInteger) index
{
    CGFloat y= (NSUInteger)(index / self.size.width);
    CGFloat x = index - y * self.size.width;
    return CGPointMake(x, y);
}


-(void) initialize
{
    for (int x = 0; x < self.size.width/2; ++x)
    {
        for(int y = 0; y < self.size.height/2; ++y)
        {
            int value = fmaxf(0, fminf(maxValue, fminf(x, y) - minSmileySize/2.0));
            [self modifyPointAtX:x andY:y withValue:value];
            [self modifyPointAtX:(int)self.size.width - x - 1 andY:y withValue:value];
            [self modifyPointAtX:x andY:(int)self.size.height - y - 1 withValue:value];
            [self modifyPointAtX:(int)self.size.width - x - 1 andY:(int)self.size.height - y - 1 withValue:value];
        }
    }
}

-(void) modifyPointAtX: (int) x andY: (int) y withValue: (int) value
{
    [locations[partitionData[x][y]] removeIndex: [self indexOfPixelAtX:x AndY:y]];
    partitionData[x][y] = value;
    [locations[partitionData[x][y]] addIndex: [self indexOfPixelAtX:x AndY:y]];
}

-(void) addSmiley: (Smiley*) smiley
{
    //Get radius for the width and height.
    float rx = smiley.width/2.0 * scale;
    float ry = smiley.height/2.0 * scale;
    
    //The greater of the two radi
    float rGreater;
    
    //Calculate the two foci points
    float f;
    
    CGPoint f1;
    CGPoint f2;
    
    CGPoint center = smiley.center;
    center.x *= scale;
    center.y *= scale;
    
    //Foci are on the x axis
    if(rx >= ry)
    {
        f = sqrtf(rx*rx - ry*ry);
        
        rGreater = rx;
        f1.x = center.x - f;
        f1.y = center.y;
        
        f2.x = center.x + f;
        f2.y = center.y;
    }
    //Foci are on the y axis
    else
    {
        f = sqrtf(ry*ry - rx*rx);
        
        rGreater = ry;
        f1.x = center.x;
        f1.y = center.y - f;
        
        f2.x = center.x;
        f2.y = center.y + f;
    }
    
    //Calculate the minimum points for where start the loop.
    CGPoint min = CGPointMake(fmaxf(0, center.x - (rGreater + minSmileySize/2.0 + maxValue)), fmaxf(0, center.y - (rGreater + minSmileySize/2.0 + maxValue)));
    //Calculate the maximum points for where to end the loop.
    CGPoint max = CGPointMake(fminf(self.size.width,center.x + rGreater + minSmileySize/2.0 + maxValue), fminf(self.size.height,center.y + rGreater + minSmileySize/2.0 + maxValue));
    
    //Loop through all the points between min and max
    CGPoint currPoint;
    for (currPoint.x = min.x; currPoint.x < max.x; ++currPoint.x)
    {
        for(currPoint.y = min.y; currPoint.y < max.y; ++currPoint.y)
        {
            //If the size at this point is zero, skip
            if(partitionData[(int)currPoint.x][(int)currPoint.y] == 0)
            {
                continue;
            }
            
            //Calculate the distance between this point and the two foci
            float dist = DistanceBetweenTwoPoints(currPoint, f1) + DistanceBetweenTwoPoints(currPoint, f2);
            dist = dist/2.0 - (rGreater + minSmileySize/2.0);
            
            //Cut the distance off between 0 and the max value, and the result is the size of the circle that can be placed at this point.
            int value = (int)roundf(fmaxf(0, fminf(maxValue, dist)));
            
            //Modify the point if the new value is lower than the previous value.
            if(partitionData[(int)currPoint.x][(int)currPoint.y] > value)
            {
                [self modifyPointAtX:currPoint.x andY:currPoint.y withValue:value];
            }
        }
    }
}

CGFloat DistanceBetweenTwoPoints(CGPoint point1,CGPoint point2)
{
    CGFloat dx = point2.x - point1.x;
    CGFloat dy = point2.y - point1.y;
    return sqrt(dx*dx + dy*dy );
};

-(CGPoint) getSmileyLocationWithMaxSizeOf: (CGFloat) maxSize andSizePointer: (CGFloat *) sizePointer
{
    CGPoint point = [self getSmileyLocationRandomlyWithMaxSizeOf:maxSize * scale andSizePointer:sizePointer];
    //getSmileyLocationRandomlyWithMaxSizeOf
    //getSmileyLocationNextToAnotherWithMaxSizeOf
    //getNextBiggestSmileyLocationWithMaxSizeOf
    
    if(point.x == -1 && point.y == -1)
    {
        return point;
    }
    
    *sizePointer /= scale;
    point.x = (point.x / scale);// + (0.5/scale);
    point.y = (point.y / scale);// + (0.5/scale);
    
    return point;
}

-(CGPoint) getSmileyLocationRandomlyWithMaxSizeOf: (CGFloat) maxSize andSizePointer: (CGFloat *) sizePointer
{
    int maxSizeIndex = maxSize/2 - minSmileySize/2 +1;
    
    NSMutableIndexSet * availableRange = [NSMutableIndexSet indexSet];
    
    
    int totalPoints = 0;
    //Check all locations between the largest and maxSize
    for(int currIndex = maxLocationIndex; currIndex >= maxSizeIndex; --currIndex)
    {
        if([locations[currIndex] count] > 0)
        {
            totalPoints += [locations[currIndex] count];
            [availableRange addIndex:currIndex];
        }
    }
    
    //Pick any location where the max smiley size will fit
    if ([availableRange count] > 0)
    {
        int randomPointIndex = (int)roundf([Random randomFloatBetween:0 and:totalPoints]);
        
        int currIndex;
        int sum = 0;
        for(currIndex = maxLocationIndex; currIndex >= maxSizeIndex && sum < randomPointIndex; --currIndex)
        {
            sum += [locations[currIndex] count];
        }
        
        NSUInteger selectedIndex = fminl(currIndex +1, maxLocationIndex);
        *sizePointer = fminf(selectedIndex * 2 + minSmileySize,maxSize);
        
        NSMutableIndexSet * currIndexSet = locations[selectedIndex];
        
        int randLocation = (int)roundf([Random randomFloatBetween:[currIndexSet firstIndex] and:[currIndexSet lastIndex]]);
        NSUInteger pixel = [currIndexSet indexLessThanOrEqualToIndex:randLocation];
        
        return [self pixelFromIndex: pixel];
    }
    
    //Nothing found between largest size and maxSize, so get the next largest possible.
    return [self getNextBiggestSmileyLocationWithMaxSizeOf: maxSize andSizePointer:sizePointer];
}

-(CGPoint) getSmileyLocationNextToAnotherWithMaxSizeOf: (CGFloat) maxSize andSizePointer: (CGFloat *) sizePointer
{
    int maxSizeIndex = maxSize/2 - minSmileySize/2 +1;
    
    //If there is a location to place a smiley with a size of maxSize
    if([locations[maxSizeIndex] count] > 0)
    {
        *sizePointer = maxSize;
        NSMutableIndexSet *currIndexSet = locations[maxSizeIndex];
        
        //Pick a random location
        int randLocation = (int)roundf([Random randomFloatBetween:[currIndexSet firstIndex] and:[currIndexSet lastIndex]]);
        NSUInteger pixel = [currIndexSet indexLessThanOrEqualToIndex:randLocation];
        
        return [self pixelFromIndex: pixel];
    }
    
    //Nothing found at this location, so get the next largest
    return [self getNextBiggestSmileyLocationWithMaxSizeOf: maxSize andSizePointer:sizePointer];
}

-(CGPoint) getNextBiggestSmileyLocationWithMaxSizeOf: (CGFloat) maxSize andSizePointer: (CGFloat *) sizePointer
{
    NSMutableIndexSet * currIndexSet = locations[maxLocationIndex];
    
    //Loop until you find a size that is placable.
    for (; maxLocationIndex > 0 && [currIndexSet count] == 0; --maxLocationIndex)
    {
        currIndexSet = locations[maxLocationIndex];
    }
    
    //Checked all possible sizes and no location found.
    if ([currIndexSet count] == 0 || maxLocationIndex == 0) {
        return CGPointMake(-1, -1);
    }
    
    //Pick largest size
    *sizePointer = fminf(maxLocationIndex*2 + minSmileySize, maxSize);
    
    //Pick a random location for this size
    int randLocation = (int)roundf([Random randomFloatBetween:[currIndexSet firstIndex] and:[currIndexSet lastIndex]]);
    NSUInteger pixel = [currIndexSet indexLessThanOrEqualToIndex:randLocation];
    
    return [self pixelFromIndex: pixel];
}

@end
