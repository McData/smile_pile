//
//  PilePartition.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/29/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Draw.h"
#import "Smiley.h"
#import "Random.h"
#include <math.h>


@interface PilePartition : NSObject

@property CGSize size;

- (id)initWithSize:(CGSize) size;

-(void) reset;
-(void) addSmiley: (Smiley*) smiley;

-(int) indexOfPixelAtX: (int) x AndY: (int) y;
-(CGPoint) pixelFromIndex: (NSUInteger) index;

-(CGPoint) getSmileyLocationWithMaxSizeOf: (CGFloat) maxSize andSizePointer: (CGFloat *) sizePointer;

@end
