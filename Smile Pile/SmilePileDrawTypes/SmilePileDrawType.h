//
//  SmilePileDrawType.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Smiley.h"
#import "SettingsData.h"
#import "Random.h"
#import "MainViewController.h"

@interface SmilePileDrawType : NSObject
{
    dispatch_queue_t drawSmilesQueue;
    
    
    NSDate * goButtonPressTime;
    
    BOOL active;
    
    MainViewController * mainView;
    
    CGRect frame;
}

@property (strong, nonatomic) UIImage * smilePileImage;

- (id) initWithView: (MainViewController * ) view dispatchQueue: (dispatch_queue_t) queue frame: (CGRect) frameRect;
-(void) setGoButtonTime: (NSDate*) goTime;
-(void) clearSmilePile;
- (void) startCreatingSmilies: (NSDate *) timeOfGoButtonPress;
- (void) resumeCreatingSmilies;
-(void) drawSmiley: (Smiley *) smiley withTime: (NSDate*) timeOfGoButtonPress;
-(void) notifyOfPileFinished;

@end
