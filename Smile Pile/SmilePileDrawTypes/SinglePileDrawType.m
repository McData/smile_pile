//
//  SinglePileDrawType.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SinglePileDrawType.h"

@implementation SinglePileDrawType

- (void) initializePile {}

- (void) finalizePile
{
    BOOL loopSetting = [((NSNumber*)[SettingsData getObjectFromIndex: kSmilePileLoopBool]) boolValue];
    if (!loopSetting)
    {
        [self notifyOfPileFinished];
    }
}

-(void) smileyLoop: (NSDate *) timeOfGoButtonPress
{
    BOOL loopSetting = [((NSNumber*)[SettingsData getObjectFromIndex: kSmilePileLoopBool]) boolValue];
    do
    {
        NSDate* timeBeforeDrawingCurrentSmiley = [NSDate date];
        
        CGPoint center = CGPointMake(frame.size.width/2, frame.size.height/2);
        
        @autoreleasepool {
            Smiley * smiley = [[Smiley alloc] initWithCenter: center];
            //Smiley * smiley = [[Smiley alloc] initSpecialWithCenter: center]; //Model citizen
            
            //Add smiley draw call to the main thread to update UI
            [self drawSmiley: smiley withTime:timeOfGoButtonPress];
        }
        
        //Sleep if looping
        if(active && loopSetting)
        {
            double sleepTime = [(NSNumber* ) [SettingsData getObjectFromIndex:kSmileyDrawSpeed] doubleValue] - [[NSDate date] timeIntervalSinceDate:timeBeforeDrawingCurrentSmiley] ;
            sleepTime = MAX(sleepTime, 0);
            [NSThread sleepForTimeInterval: sleepTime];
            
            [self clearSmilePile];
        }
        
    }  while (active && loopSetting);
}

-(void) drawModelCitizen
{
    dispatch_async(drawSmilesQueue, ^{
        goButtonPressTime = [NSDate date];
        
        CGPoint center = CGPointMake(frame.size.width/2, frame.size.height/2);

        @autoreleasepool {
            Smiley * smiley = [[Smiley alloc] initSpecialWithCenter: center];
            
            //Add smiley draw call to the main thread to update UI
            [self drawSmiley: smiley withTime: goButtonPressTime];
        }
    });
}

@end
