//
//  OverlappingPileDrawType.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "OverlappingPileDrawType.h"

@implementation OverlappingPileDrawType
{
    long currentSmileyCount;
}

- (void) initializePile
{
    currentSmileyCount = 0;
}
- (void) finalizePile
{
    
}

- (void) smileyLoop: (NSDate *) timeOfGoButtonPress
{
    for (; active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime]; ++currentSmileyCount)
    {
        NSDate* timeBeforeDrawingCurrentSmiley = [NSDate date];
        
        CGFloat size = [Random randomFloatBetween: [[SettingsData getObjectFromIndex:kMinSmileySize] floatValue] and:[[SettingsData getObjectFromIndex:kSmileySize] floatValue]];
        
        CGPoint center = CGPointMake([Random randomFloatBetween:size/3 and:frame.size.width - size/3], [Random randomFloatBetween:size/3 and:frame.size.height- size/3]);
        
        @autoreleasepool {
            Smiley * currentSmiley = [[Smiley alloc] initWithCenter: center andSize: size];
            
            //Add smiley draw call to the main thread to update UI
            [self drawSmiley: currentSmiley withTime:timeOfGoButtonPress];
        }
        
        
        if(active && [timeOfGoButtonPress isEqualToDate:goButtonPressTime])
        {
            //Sleep thread so that all smiles do not appear at once
            //Sleep based on settings amount.
            double sleepTime = [(NSNumber* ) [SettingsData getObjectFromIndex:kSmileyDrawSpeed] doubleValue] - [[NSDate date] timeIntervalSinceDate:timeBeforeDrawingCurrentSmiley] ;
            sleepTime = MAX(sleepTime, 0);
            [NSThread sleepForTimeInterval: sleepTime];
        }
    }
}


@end
