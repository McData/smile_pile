//
//  SettingsData.h
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSmilePileSettings @"SmilePileSettings"

#define kSmileySize @"SmileySize"

#define kJelloMinAmount @"JelloMinAmount"
#define kJelloMaxAmount @"JelloMaxAmount"
#define kJelloAmount @"JelloAmount"

#define kMaxSmileySize @"MaxSmileySize"
#define kMinSmileySize @"MinSmileySize"

#define kSmilePileType @"SmilePileType"
#define kSingleSmileyPileType @"SingleSmileyPileType"
#define kOverlappingPileType @"OverlappingPileType"
#define kSeparatedPileType @"SeparatedPileType"

#define kSmileyMinDrawSpeed @"SmileyMinDrawSpeed"
#define kSmileyMaxDrawSpeed @"SmileyMaxDrawSpeed"
#define kSmileyDrawSpeed @"SmileyDrawSpeed"

#define kSmilePileLoopBool @"SmilePileLoopBool"

#define kColorSettings @"ColorSettings"

#define kDrawSmileOnTouch @"DrawSmileOnTouch"
#define kTouchSmileySetSize @"TouchSmileySize"

@interface SettingsData : NSObject

+(NSMutableDictionary *) getAllSettings;
+(void) setObject: (id) object forSetting: (NSString*) index;
+(id) getObjectFromIndex: (NSString* ) index;

+(void) loadSettings;
+(void) saveSettings;

+(void) revertToDefault;

@end
