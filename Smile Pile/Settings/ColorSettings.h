//
//  ColorSettings.h
//  Smile Pile
//
//  Created by Joshua Linge on 8/4/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RandomColorCollection.h"

@interface ColorSettings : NSObject <NSCoding>

-(void) revertToDefault;

-(UIColor*) getColorPresetForIndex: (NSInteger) index;
-(UIColor*) getColorHistoryForIndex: (NSInteger) index;
-(UIColor*) getSelectedRandomColorForIndex: (NSInteger) index;

@property UIColor * smileyColor;
@property UIColor * backgroundColor;
@property BOOL useRandomSmileyColors;
@property NSInteger selectedRandomColors;

-(UIColor *) getSmileyColor;

+(CGFloat) getColorBrigtnessForColor:(UIColor*) color;
+(UIColor *) getOutlineColorForSmileyColor:(UIColor *) smileyColor;

-(NSUInteger) SelectedRandomColorCount;
-(void) addSelectedRandomColor: (UIColor *) newColor;
-(void) removeSelectedRandomColorAtIndex: (NSInteger) index;
-(void) replaceSelectedRandomColor: (UIColor *) newColor atIndex: (NSInteger) index;
-(void) setSelectedRandomColors: (NSInteger) index;

-(NSInteger) RandomColorCount;
-(RandomColorCollection *) getRandomColorCollectionForIndex: (NSInteger) index;
-(RandomColorCollection *) createColorCollectionWithName:(NSString*) name;
-(void) removeColorCollectionAtIndex: (NSInteger) index;

-(NSUInteger) ColorHistoryCount;
-(void) addColorToColorHistory: (UIColor *) newColor;

+(NSMutableArray *) decodeColorArray:(NSMutableArray *) codedColorArray;
+(NSMutableArray *) encodeColorArray:(NSMutableArray *) decodedColorArray;

extern NSString * const keySmileyColor;
extern NSString * const keyBackgroundColor;
extern NSString * const keyColorHistory;
extern NSString * const keySelectedRandomColors;
extern NSString * const keyRandomColors;
//extern NSString * const key;

@end
