//
//  SettingsData.m
//  SmilePileP1
//
//  Created by Joshua Linge on 5/20/14.
//  Copyright (c) 2014 Joshua Linge. All rights reserved.
//

#import "SettingsData.h"
#import "ColorSettings.h"

@implementation SettingsData

static NSMutableDictionary * data = nil;
static BOOL displayDebugOutput = NO;

+(NSMutableDictionary *) getAllSettings
{
    if(data == nil)
    {
        [self loadSettings];
    }
    
    return data;
}

+(void) setObject: (id) object forSetting: (NSString*) index
{
    [data setObject:object forKey:index];
}

+(id) getObjectFromIndex: (NSString* ) index
{
    return [data objectForKey:index];
}

+(void) setMissingValue:(NSString*) value withObject:(id) object
{
    if([data objectForKey:value] == nil)
    {
        if (displayDebugOutput)
        {
            NSLog(@"Value %@ added", value);
        }
        [data setObject: object forKey: value];
    }
}

+(void) initializeMissingData
{
    //[self setMissingValue:  withObject: ];
    
    [self setMissingValue: kMinSmileySize withObject:[NSNumber numberWithFloat: 25.0f]];
    [self setMissingValue: kMaxSmileySize withObject:[NSNumber numberWithFloat: 300.0f]];
    
    float defaultSmileySize;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        defaultSmileySize = 125.0f; //100
    }
    else
    {
        defaultSmileySize = 225.0f;
    }
    [self setMissingValue: kSmileySize withObject: [NSNumber numberWithFloat: defaultSmileySize]];
    
    
    [self setMissingValue: kJelloMinAmount withObject: [NSNumber numberWithFloat: 1.0f]];
    [self setMissingValue: kJelloMaxAmount withObject: [NSNumber numberWithFloat: 2.0f]];
    [self setMissingValue: kJelloAmount withObject: [NSNumber numberWithFloat: 1.150f]];
    
    [self setMissingValue: kSmilePileType withObject: kSeparatedPileType];
    //kSingleSmileyPileType, kSeparatedPileType, kOverlappingPileType
    
    [self setMissingValue: kSmileyMinDrawSpeed withObject: [NSNumber numberWithFloat: 0.15f]];
    [self setMissingValue: kSmileyMaxDrawSpeed withObject: [NSNumber numberWithFloat: 1.0f]];
    
    [self setMissingValue: kSmileyDrawSpeed withObject: [NSNumber numberWithFloat: 0.5f]];
    
    
    [self setMissingValue: kSmilePileLoopBool withObject: [NSNumber numberWithBool:NO]];
    
    [self setMissingValue: kColorSettings withObject: [[ColorSettings alloc] init]];
    
    [self setMissingValue: kDrawSmileOnTouch withObject: [NSNumber numberWithBool: NO]];
    [self setMissingValue: kTouchSmileySetSize withObject: [NSNumber numberWithBool:YES]];
}

+(void) loadSettings
{
    //Load from user defaults.
    NSDictionary * loadedData = [[NSUserDefaults standardUserDefaults] objectForKey: kSmilePileSettings];
    //loadedData = nil;
    
    //Data not found, initialize with default values.
    if(loadedData == nil)
    {
        NSLog(@"Initializing data with default values");
        
        data = [[NSMutableDictionary alloc] init];
        
        displayDebugOutput = NO;
        [self initializeMissingData];
    }
    
    //Data found
    else
    {
        NSLog(@"Loading data from User Defaults");
        
        data = [[NSMutableDictionary alloc] initWithDictionary: loadedData];
        
        if([data objectForKey:kColorSettings] != nil)
        {
            //Unarchive color settings objects
            [data setObject: (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData: [data objectForKey:kColorSettings]] forKey: kColorSettings];
        }
        
        displayDebugOutput = YES;
        [self initializeMissingData];
        displayDebugOutput = NO;
    }
    
}

+(void) saveSettings
{
    ColorSettings * colorSettings = [data objectForKey:kColorSettings];
    
    //Replace color settings object with a saved form.
    [data setObject: [NSKeyedArchiver archivedDataWithRootObject: colorSettings] forKey: kColorSettings];
    
    //Save to user defaults
    [[NSUserDefaults standardUserDefaults] setObject: data forKey:kSmilePileSettings];
    
    //Undo archiving.
    [data setObject: colorSettings forKey: kColorSettings];
}

+(void) revertToDefault
{
    ColorSettings * colorSettings = [data objectForKey:kColorSettings];
    [colorSettings revertToDefault];
    
    [data removeAllObjects];
    
    [data setObject: colorSettings forKey: kColorSettings];
    
    displayDebugOutput = NO;
    [self initializeMissingData];
}

@end
