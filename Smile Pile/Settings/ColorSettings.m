//
//  ColorSettings.m
//  Smile Pile
//
//  Created by Joshua Linge on 8/4/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "ColorSettings.h"
#import "SettingsData.h"
#import "Random.h"
#import "math.h"
//#import "RandomColorCollection.h"

@implementation ColorSettings
{
    UIColor * colorPresets[8];
    NSMutableArray * colorHistory;
    
    NSMutableArray * randomColors;
}

//Constants
NSString* const keySmileyColor = @"SmileyColor";
NSString* const keyBackgroundColor = @"BackgroundColor";
NSString* const keyColorHistory = @"ColorHistory";
NSString* const keySelectedRandomColors = @"SelectedRandomColors";
NSString* const keyRandomColors = @"RandomColors";


- (id)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void) initialize
{
    [self initializeColorPresets];
    [self initializeColors];
    [self initialzieColorHistory];
    [self initializeRandomColorSelections];
}

-(void) initializeColors
{
    self.smileyColor = colorPresets[0];
    self.backgroundColor = colorPresets[6];
}

-(void) initializeColorPresets
{
    colorPresets[0] = [UIColor colorWithRed:255/255.0 green:204/255.0 blue:0/255.0   alpha:1]; //YellowOrange color
    colorPresets[1] = [UIColor colorWithRed:255/255.0 green:149/255.0 blue:0/255.0   alpha:1]; //Orange
    colorPresets[2] = [UIColor colorWithRed:255/255.0 green:59/255.0  blue:48/255.0  alpha:1]; //Red
    colorPresets[3] = [UIColor colorWithRed:0/255.0   green:122/255.0 blue:255/255.0 alpha:1]; //Blue
    colorPresets[4] = [UIColor colorWithRed:89/255.0  green:200/255.0 blue:250/255.0 alpha:1]; //Light Blue
    colorPresets[5] = [UIColor colorWithRed:75/255.0  green:217/255.0 blue:100/255.0 alpha:1]; //Green
    colorPresets[6] = [UIColor colorWithRed:1 green:1 blue:1 alpha:1]; //White
    colorPresets[7] = [UIColor colorWithRed:0 green:0 blue:0 alpha:1]; //Black
}

-(void) initialzieColorHistory
{
    colorHistory = [NSMutableArray arrayWithCapacity:8];
    [colorHistory addObject: self.smileyColor];
    [colorHistory addObject: self.backgroundColor];
}

-(void) initializeRandomColorSelections
{
    self.useRandomSmileyColors = NO;
    
    self.selectedRandomColors = 0;
   
    randomColors = [NSMutableArray array];
  
    [self addRandomColorsMissingPresets];
}

-(void) addRandomColorsMissingPresets
{
    RandomColorCollection * randomColorCollectionPreset = [self getPresetPreset];
    if (randomColors.count == 0 || ![((RandomColorCollection*)randomColors[0]).name isEqualToString:randomColorCollectionPreset.name])
    {
        [randomColors insertObject:randomColorCollectionPreset atIndex:0];
    }
    
    randomColorCollectionPreset = [self getGrayscalePreset];
    if (randomColors.count == 1 || ![((RandomColorCollection*)randomColors[1]).name isEqualToString:randomColorCollectionPreset.name])
    {
        [randomColors insertObject:randomColorCollectionPreset atIndex:1];
    }
    
    randomColorCollectionPreset = [self getPastelsPreset];
    if (randomColors.count == 2 || ![((RandomColorCollection*)randomColors[2]).name isEqualToString:randomColorCollectionPreset.name])
    {
        [randomColors insertObject:randomColorCollectionPreset atIndex:2];
    }
}


-(RandomColorCollection *) getPresetPreset
{
    RandomColorCollection * presetCollection = [[RandomColorCollection alloc] initWithName:@"Preset" andColors:[NSMutableArray arrayWithObjects:colorPresets count:8]];
    return  presetCollection;
}

-(RandomColorCollection *) getGrayscalePreset
{
    NSMutableArray * grayColors = [NSMutableArray array];
    
    for(int currColor = 0; currColor < 9; ++currColor)
    {
        [grayColors addObject:[UIColor colorWithRed:(currColor*32/256.0f) green:(currColor*32/256.0f) blue:(currColor*32/256.0f) alpha:1]];
    }
    
    RandomColorCollection * grayscaleCollection = [[RandomColorCollection alloc] initWithName:@"Grayscale" andColors:grayColors];
    return grayscaleCollection;
}

//TODO
-(RandomColorCollection *) getBluePreset
{
    NSMutableArray * blueColors = [NSMutableArray array];
    
    //[blueColors addObject:[UIColor colorWithRed:00/255.0f green:00/255.0f blue:00/255.0f alpha:1]];
    
    RandomColorCollection * blueCollection = [[RandomColorCollection alloc] initWithName:@"Blues" andColors:blueColors];
    return blueCollection;
}

-(RandomColorCollection *) getPastelsPreset
{
    NSMutableArray * pastelColors = [NSMutableArray array];
    
    [pastelColors addObject:[UIColor colorWithRed:3/255.0f green:192/255.0f blue:60/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:119/255.0f green:221/255.0f blue:119/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:194/255.0f green:59/255.0f blue:34/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:244/255.0f green:154/255.0f blue:194/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:207/255.0f green:207/255.0f blue:196/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:253/255.0f green:253/255.0f blue:150/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:150/255.0f green:111/255.0f blue:214/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:222/255.0f green:165/255.0f blue:164/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:203/255.0f green:153/255.0f blue:201/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:100/255.0f green:20/255.0f blue:100/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:179/255.0f green:158/255.0f blue:181/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:255/255.0f green:179/255.0f blue:71/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:174/255.0f green:198/255.0f blue:207/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:130/255.0f green:105/255.0f blue:83/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:255/255.0f green:105/255.0f blue:97/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:119/255.0f green:158/255.0f blue:203/255.0f alpha:1]];
    [pastelColors addObject:[UIColor colorWithRed:255/255.0f green:209/255.0f blue:220/255.0f alpha:1]];
    
    RandomColorCollection * pastelCollection = [[RandomColorCollection alloc] initWithName:@"Pastels" andColors:pastelColors];
    return pastelCollection;
}


-(void) revertToDefault
{
    [self initializeColors];
    self.useRandomSmileyColors = NO;
    self.selectedRandomColors = 0;
}


-(UIColor*) getColorPresetForIndex: (NSInteger) index
{
    if(index >= 8 || index < 0)
        return nil;
    
    return colorPresets[index];
}

-(UIColor*) getColorHistoryForIndex: (NSInteger) index
{
    if(index >= [colorHistory count] || index < 0)
    {
        return nil;
    }
    return colorHistory[index];
}

-(UIColor*) getSelectedRandomColorForIndex: (NSInteger) index
{
    if(index >= [((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors count] || index < 0)
    {
        return nil;
    }
    return ((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors[index];
}


-(UIColor *) getSmileyColor
{
    //if random color selected, get new color
    if(self.useRandomSmileyColors)
    {
        if([((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors count] == 0)
        {
            self.selectedRandomColors = 0;
        }
        
        RandomColorCollection * selectedCollection =((RandomColorCollection *)randomColors[self.selectedRandomColors]);
        
        return selectedCollection.colors[(int)floorf([Random randomFloatBetween:0 and:[selectedCollection.colors count]])];
    }
    else
    {
        return self.smileyColor;
    }
}

+(CGFloat) getColorBrigtnessForColor:(UIColor*) color
{
    const CGFloat *componentColors = CGColorGetComponents(color.CGColor);
    
    CGFloat colorBrightness = ((componentColors[0] * 299) + (componentColors[1] * 587) + (componentColors[2] * 114)) / 1000;
    
    return colorBrightness;
}


+(UIColor *) getOutlineColorForSmileyColor:(UIColor *) smileyColor
{
    CGFloat colorBrightness = [self getColorBrigtnessForColor:smileyColor];
    
    //NSLog(@"%f", colorBrightness);
    
    //Smiley color is dark
    if (colorBrightness < 0.15)
    {
        return [UIColor whiteColor];
    }
    //Smiley color is light.
    else
    {
        return [UIColor blackColor];
    }
    
}


-(NSUInteger) SelectedRandomColorCount
{
    return [((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors count];
}

-(void) addSelectedRandomColor: (UIColor *) newColor
{
    [((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors addObject:newColor];
}

-(void) removeSelectedRandomColorAtIndex: (NSInteger) index
{
    [((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors removeObjectAtIndex:index];
}

-(void) replaceSelectedRandomColor: (UIColor *) newColor atIndex: (NSInteger) index
{
    [((RandomColorCollection *)randomColors[self.selectedRandomColors]).colors replaceObjectAtIndex:index withObject:newColor];
}

-(NSInteger) RandomColorCount
{
    return [randomColors count];
}

-(RandomColorCollection *) getRandomColorCollectionForIndex: (NSInteger) index
{
    return randomColors[index];
}

-(RandomColorCollection *) createColorCollectionWithName:(NSString*) name
{
    RandomColorCollection * newCollection = [[RandomColorCollection alloc] initWithName:name];
    [randomColors addObject: newCollection];
    return newCollection;
}

-(void) removeColorCollectionAtIndex: (NSInteger) index
{
    if(self.selectedRandomColors == index)
    {
        self.selectedRandomColors = 0;
    }
    else if (self.selectedRandomColors > index)
    {
        --self.selectedRandomColors;
    }
    
    [randomColors removeObjectAtIndex:index];
}


-(NSUInteger) ColorHistoryCount
{
    return [colorHistory count];
}



-(void) addColorToColorHistory: (UIColor *) newColor
{
    for(int currentColor = 0; currentColor < [colorHistory count]; ++currentColor)
    {
        if(CGColorEqualToColor(newColor.CGColor,((UIColor*)[colorHistory objectAtIndex:currentColor]).CGColor))
        {
            [colorHistory removeObjectAtIndex:currentColor];
        }
    }
    
    if([colorHistory count] == 8)
    {
        [colorHistory removeObjectAtIndex:7];
    }
    
    [colorHistory insertObject:newColor atIndex:0];
}




+(NSMutableArray *) decodeColorArray:(NSMutableArray *) codedColorArray
{
    NSMutableArray * decodedColorArray = [NSMutableArray arrayWithArray:codedColorArray];
    for(int curr = 0; curr < [codedColorArray count]; ++curr)
    {
        decodedColorArray[curr] = [NSKeyedUnarchiver unarchiveObjectWithData: codedColorArray[curr]];
    }
    return decodedColorArray;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        [self initializeColorPresets];
        
        self.smileyColor = [decoder decodeObjectForKey:keySmileyColor];
        self.backgroundColor = [decoder decodeObjectForKey:keyBackgroundColor];
        colorHistory = [ColorSettings decodeColorArray:[decoder decodeObjectForKey:keyColorHistory]];
       
        self.selectedRandomColors = [[decoder decodeObjectForKey:keySelectedRandomColors] integerValue];
        
        //Get random colors data
        id randomColorsData = [decoder decodeObjectForKey:keyRandomColors];
        
        //If no data was found or data is old format, create and initialize it
        if(randomColorsData == nil || [randomColorsData[0] isKindOfClass:[NSData class]])
        {
            [self initializeRandomColorSelections];
        }
        //Load data if found
        else
        {
            randomColors = randomColorsData;
            [self addRandomColorsMissingPresets];
        }
        // = [decoder decodeObjectForKey: ];
    }
    return self;
}

+(NSMutableArray *) encodeColorArray:(NSMutableArray *) decodedColorArray
{
    NSMutableArray * codedColorArray = [NSMutableArray arrayWithArray:decodedColorArray];
    for(int curr = 0; curr < [decodedColorArray count]; ++curr)
    {
        codedColorArray[curr] = [NSKeyedArchiver archivedDataWithRootObject: decodedColorArray[curr]];
    }
    return codedColorArray;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.smileyColor forKey:keySmileyColor];
    [encoder encodeObject:self.backgroundColor forKey:keyBackgroundColor];
    [encoder encodeObject:[NSNumber numberWithInteger:self.selectedRandomColors] forKey:keySelectedRandomColors];
    [encoder encodeObject: [ColorSettings encodeColorArray:colorHistory] forKey:keyColorHistory];
    [encoder encodeObject: randomColors forKey:keyRandomColors];
    
    //[encoder encodeObject: forKey:];
}

@end
