//
//  RandomColorCollection.h
//  Smile Pile
//
//  Created by Joshua Linge on 8/11/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RandomColorCollection : NSObject <NSCoding>

@property NSString * name;
@property BOOL isEditable;
@property NSMutableArray * colors;

- (id)initWithName: (NSString*) name;
- (id)initWithName: (NSString*) name andColors: (NSArray *) colors;

extern NSString * const keyName;
extern NSString * const keyIsEditable;
extern NSString * const keyColors;

@end
