//
//  RandomColorCollection.m
//  Smile Pile
//
//  Created by Joshua Linge on 8/11/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "RandomColorCollection.h"
#import "ColorSettings.h"

@implementation RandomColorCollection

NSString* const keyName = @"Name";
NSString* const keyIsEditable = @"IsEditable";
NSString* const keyColors = @"Colors";

- (id)initWithName: (NSString*) name
{
    self = [super init];
    if (self) {
        self.name = name;
        self.isEditable = YES;
        self.colors = [NSMutableArray array];
    }
    return self;
}

- (id)initWithName: (NSString*) name andColors: (NSArray *) colors
{
    self = [super init];
    if (self) {
        self.name = name;
        self.isEditable = NO;
        self.colors = [NSMutableArray arrayWithArray:colors];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        
        self.name = [decoder decodeObjectForKey:keyName];
        self.isEditable = [[decoder decodeObjectForKey:keyIsEditable] boolValue];
        self.colors = [ColorSettings decodeColorArray:[decoder decodeObjectForKey:keyColors]];
        // = [decoder decodeObjectForKey: ];
    }
    return self;
}


- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:keyName];
    [encoder encodeObject:[NSNumber numberWithBool:self.isEditable] forKey:keyIsEditable];
    [encoder encodeObject: [ColorSettings encodeColorArray:self.colors] forKey:keyColors];
    
    //[encoder encodeObject: forKey:];
}

@end
