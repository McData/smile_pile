//
//  ViewRandomColorColectionsViewController.h
//  Smile Pile
//
//  Created by Joshua Linge on 8/11/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewRandomColorColectionsViewController : UITableViewController <UIAlertViewDelegate>

@property (weak, nonatomic) UILabel * currentColorCollectionNameLabel;

@end
