//
//  MainViewController.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "MainViewController.h"
#import "Smiley.h"
#import "SettingsData.h"
#import "ColorSettings.h"
#import "Random.h"
#import "PilePartition.h"
#import "SmilePileDrawType.h"
#import "SinglePileDrawType.h"
#import "OverlappingPileDrawType.h"
#import "SeparatedPileDrawType.h"

@interface MainViewController ()
{
    dispatch_queue_t drawSmilesQueue;
    NSDate * goButtonPressTime;
    
    CGRect frame;
    NSMutableDictionary * pileTypeMap;
    
    BOOL initialized;
    
    BOOL active;
    NSString * currentActiveSetting;
    
    NSMutableArray * toolBarItems;
    
    NSDate * timeOfLastTouchSmileyDraw;
}

@property (weak, nonatomic) IBOutlet UIImageView *ImageView;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property ( nonatomic) IBOutlet UIBarButtonItem *playButton;
@property ( nonatomic) IBOutlet UIBarButtonItem *pauseButton;
@property ( nonatomic) IBOutlet UIBarButtonItem *gapItem;

@end

@implementation MainViewController

static const float DELAY_BETWEEN_TOUCH_SMILEY_DRAWS = 0.1f;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

//Used for initialization after all geometry has been calculated
-(void)viewDidLayoutSubviews
{
    if(!initialized) {
        //If GCD queue has not been created, create it.
        if (!drawSmilesQueue)
        {
            drawSmilesQueue = dispatch_queue_create("com.SmilePile.SmiliesQueue", NULL);
        }
        
        //Display model citizen image at start
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            //4 inch iphone
            if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
            {
                self.ImageView.image = [UIImage imageNamed:@"iPhone 5 Model Citizen Start"];
            }
            //3.5 inch iphone
            else
            {
                self.ImageView.image = [UIImage imageNamed:@"iPhone Model Citizen Start"];
            }
        }
        else
        {
            self.ImageView.image = [UIImage imageNamed:@"iPad Model Citizen Start"];
        }
        
        //Load settings data for the app
        [SettingsData loadSettings];
        
        //Create a variable to hold the rectanlge for the display image
        frame = CGRectMake(0, 0, self.ImageView.frame.size.width, self.ImageView.frame.size.height);
        
        //Create each pile type object;
        pileTypeMap = [NSMutableDictionary dictionary];
        pileTypeMap[kSingleSmileyPileType] = [[SinglePileDrawType alloc]initWithView:self dispatchQueue:drawSmilesQueue frame:frame];
        pileTypeMap[kOverlappingPileType] = [[OverlappingPileDrawType alloc]initWithView:self dispatchQueue:drawSmilesQueue frame:frame];
        pileTypeMap[kSeparatedPileType] = [[SeparatedPileDrawType alloc]initWithView:self dispatchQueue:drawSmilesQueue frame:frame];
        
        //[pileTypeMap[kSingleSmileyPileType] drawModelCitizen];
                       
        initialized = YES;
        
        currentActiveSetting = nil;
        active = NO;
        
        
        //Setup the toolbar to the inital setting (Play button available)
        toolBarItems = [NSMutableArray arrayWithArray:self.toolBar.items];
        [toolBarItems removeObject:self.pauseButton];
        [toolBarItems removeObject:self.gapItem];

        [self.toolBar setItems:toolBarItems];
    }
    
    timeOfLastTouchSmileyDraw = [NSDate date];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if ([ColorSettings getColorBrigtnessForColor:self.view.backgroundColor] < 0.50) {
        return UIStatusBarStyleLightContent;
    }
    
    return UIStatusBarStyleDefault;
}


- (void)viewWillAppear:(BOOL)animated
{
    //Pile setting changed while the view was not displayed.
    //Finish the current pile and allow the user to start a new one for the new pile type
    if(currentActiveSetting != nil && ![currentActiveSetting isEqualToString:[SettingsData getObjectFromIndex: kSmilePileType]])
    {
        [self pileFinished];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    //Stop pile from drawing while on another screen
    if (active)
    {
        [self pauseAction:nil];
    }
}


-(void) drawSmileyBasedOnTouch: (UITouch *) touch
{
    timeOfLastTouchSmileyDraw = [NSDate date];
    
    CGPoint center = [touch locationInView:self.ImageView];
    CGFloat size = [[SettingsData getObjectFromIndex:kSmileySize] floatValue];
    
    if(![[SettingsData getObjectFromIndex:kTouchSmileySetSize] boolValue]) {
        size = [Random randomFloatBetween:[[SettingsData getObjectFromIndex:kMinSmileySize] floatValue] and:size];
    }
    
    Smiley * touchSmiley = [[Smiley alloc] initWithCenter:center andSize:size];
    
    [pileTypeMap[currentActiveSetting] drawSmiley:touchSmiley withTime: goButtonPressTime];
    
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    BOOL drawOnTouch = [[SettingsData getObjectFromIndex:kDrawSmileOnTouch] boolValue];
    
    double timeSinceLastTouchSmiley = [[NSDate date] timeIntervalSinceDate:timeOfLastTouchSmileyDraw];
    
    NSString * currentSetting = [SettingsData getObjectFromIndex: kSmilePileType];
    
    if ([currentSetting isEqualToString:kOverlappingPileType] && drawOnTouch && (timeSinceLastTouchSmiley > DELAY_BETWEEN_TOUCH_SMILEY_DRAWS)) {
        
        //No current pile, set pile to overlapping and redraw the screen.
        if(currentActiveSetting == nil)
        {
            [self updateDisplayedImage:[Draw imageWithColor:[UIColor clearColor] andBounds:frame]];
            self.view.backgroundColor = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor;
            [self setNeedsStatusBarAppearanceUpdate];
            
            //Save time of button press
            goButtonPressTime = [NSDate date];
            
            currentActiveSetting = kOverlappingPileType;
            
            [pileTypeMap[kOverlappingPileType] setGoButtonTime: goButtonPressTime];
        }
        
        UITouch * touch = [touches anyObject];
        [self drawSmileyBasedOnTouch:touch];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
    
    BOOL drawOnTouch = [[SettingsData getObjectFromIndex:kDrawSmileOnTouch] boolValue];
    
    double timeSinceLastTouchSmiley = [[NSDate date] timeIntervalSinceDate:timeOfLastTouchSmileyDraw];
    
    NSString * currentSetting = [SettingsData getObjectFromIndex: kSmilePileType];
    
    if ([currentSetting isEqualToString:kOverlappingPileType] && drawOnTouch && (timeSinceLastTouchSmiley > DELAY_BETWEEN_TOUCH_SMILEY_DRAWS)) {
        
        UITouch * touch = [touches anyObject];
        [self drawSmileyBasedOnTouch:touch];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
}

-(IBAction)clearButtonPressed:(id)sender
{
    //Stop drawing the current pile
    [pileTypeMap[currentActiveSetting] setActive: NO];

    //Display a blank image and set the background color based on the setting.
    [self updateDisplayedImage:[Draw imageWithColor:[UIColor clearColor] andBounds:frame]];
    self.view.backgroundColor = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor;
    [self setNeedsStatusBarAppearanceUpdate];
    
    //Set the pile as being completed
    [self pileFinished];
}



-(IBAction)playAction:(id)sender
{
    //Update toolbar to display pause button instead of play button
    [self switchFromPlayButtonToPauseButtonOnToolbar];
    
    //Start drawing a new pile
    if(currentActiveSetting == nil)
    {
        [self updateDisplayedImage:[Draw imageWithColor:[UIColor clearColor] andBounds:frame]];
        self.view.backgroundColor = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor;
        [self setNeedsStatusBarAppearanceUpdate];
    
        //Save time of button press
        goButtonPressTime = [NSDate date];
        
        //Tell the current pile type to create smilies.
        currentActiveSetting = [SettingsData getObjectFromIndex: kSmilePileType];
        [pileTypeMap[currentActiveSetting] startCreatingSmilies: goButtonPressTime];
    }
    //Resume drawing an old pile
    else
    {
        [pileTypeMap[currentActiveSetting] resumeCreatingSmilies];
    }
    
    active = YES;
}

-(IBAction)pauseAction:(id)sender
{
    //Stop the current active pile
    [pileTypeMap[currentActiveSetting] setActive:NO];
    
    //Update toolbar to display play button instead of pause button
    [self switchFromPauseButtonToPlayButtonOnToolBar];
   
    active = NO;
}


-(void) switchFromPlayButtonToPauseButtonOnToolbar
{
    if(active)
        return;
    
    //Remove the play button and replace it with the pause button.
    [toolBarItems setObject:self.pauseButton atIndexedSubscript:2];
    
    //Insert a 1 pixel gap after the pause button to keep button spaced equally.
    [toolBarItems insertObject:self.gapItem atIndex:3];
    
    //Update the toolbar
    [self.toolBar setItems:toolBarItems animated:NO];
}

-(void) switchFromPauseButtonToPlayButtonOnToolBar
{
    if(!active)
        return;
    
    //Remove the pause button and replace it with the play button.
    [toolBarItems setObject:self.playButton atIndexedSubscript:2];
    
    //Remove the 1 pixel gap after the play button to keep button spaced equally.
    [toolBarItems removeObjectAtIndex:3];
    
    //Update the toolbar
    [self.toolBar setItems:toolBarItems animated:NO];
}


-(void) updateDisplayedImage: (UIImage *) newImage
{
    //Set the display image to the given image
    self.ImageView.image = newImage;
}

-(void) pileFinished
{
    //If currently active, update toolbar to display pile has finished.
    if(active) {
        //Update toolbar to display play button instead of pause button
        [self switchFromPauseButtonToPlayButtonOnToolBar];
    }
    
    [pileTypeMap[currentActiveSetting] clearSmilePile];
    
    active = NO;
    currentActiveSetting = nil;
}


-(UIImage *) getSavableImage
{
    UIImage * savableImage;
    
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, NO, 0.0);
    } else {
        UIGraphicsBeginImageContext(self.view.frame.size);
    }
    
    [self.view.backgroundColor setFill];
    UIRectFill(self.view.frame);
    
    [self.ImageView.image drawInRect:self.ImageView.frame];
    
    savableImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return savableImage;
}

- (IBAction)shareButtonAction:(id)sender {
    
    [self pauseAction:nil];
    
    UIImage * imageToSave = [self getSavableImage];
    
    NSArray *activityItems = [NSArray arrayWithObjects: @"My Smile Pile!", imageToSave, nil];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:activityController animated:YES completion:nil];
    }
    else //Ipad action
    {
        //Dismiss active popover before creating a new popover.
        if(self.currentPopoverController) {
            [self.currentPopoverController dismissPopoverAnimated:YES];
        }
        
        self.currentPopoverController = [[UIPopoverController alloc] initWithContentViewController:activityController];
        self.currentPopoverController.delegate = self;
        [self.currentPopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}



#pragma mark - Settings Table View Controller


- (void)settingsTableViewControllerDidFinish:(SettingsTableViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        self.currentPopoverController = nil;
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.currentPopoverController = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        
        UINavigationController * navigationController = [segue destinationViewController];
        [[[navigationController viewControllers] objectAtIndex:0] setDelegate:self];

        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            self.currentPopoverController = [(UIStoryboardPopoverSegue *)segue popoverController];
            self.currentPopoverController.delegate = self;
        }
    }
}

- (IBAction)togglePopover:(id)sender
{
    if (self.currentPopoverController) {
        [self.currentPopoverController dismissPopoverAnimated:YES];
        self.currentPopoverController = nil;
    }
    else {
        [self performSegueWithIdentifier:@"showAlternate" sender:sender];
    }
}

@end
