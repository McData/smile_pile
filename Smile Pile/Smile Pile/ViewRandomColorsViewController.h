//
//  ViewRandomColorsViewController.h
//  Smile Pile
//
//  Created by Joshua Linge on 8/10/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerViewController.h"
#import "RandomColorCollection.h"

@interface ViewRandomColorsViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, ColorPickerDelegate>

@property (weak, nonatomic) RandomColorCollection * colorCollection;

@end
