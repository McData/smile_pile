//
//  ColorPickerViewController.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/26/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ColorPickerViewController;

@protocol ColorPickerDelegate
- (void)colorSelected:(UIColor *) selectedColor;
@end


@interface ColorPickerViewController : UIViewController

@property (weak, nonatomic) id <ColorPickerDelegate> delegate;

@property UIColor * color;

@end
