//
//  ViewRandomColorsViewController.m
//  Smile Pile
//
//  Created by Joshua Linge on 8/10/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "ViewRandomColorsViewController.h"
#import "SettingsData.h"
#import "ColorSettings.h"

@interface ViewRandomColorsViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@end

@implementation ViewRandomColorsViewController
{
    BOOL editing;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.editButton setEnabled:self.colorCollection.isEditable];
    
    [self.navigationItem setTitle:self.colorCollection.name];
    
    //editing = self.colorCollection.colors.count == 0;
    //[self.editButton setTitle:(editing?@"Done":@"Remove")];
    
}

#pragma mark UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.colorCollection.colors count] + (editing || !self.colorCollection.isEditable? 0: 1);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [self.colorCollection.colors count])
    {
        //Create a blank cell
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LabelCell" forIndexPath:indexPath];
        
        UIColor * color = self.colorCollection.colors[indexPath.row];
        cell.backgroundColor = color;
        
        UILabel* label = (UILabel*)[cell viewWithTag:100];
        
        //Set label to the minus button but only show it when editing
        [label setText:@"–"];
        //[label setHidden: !editing];
        label.textColor = editing?[ColorSettings getOutlineColorForSmileyColor:color]: color;
        
        
        [cell.layer setBorderColor: [[UIColor blackColor] CGColor]];
        [cell.layer setBorderWidth: 1.0];
        
        return cell;
    }
    else
    {
        //Create a blank cell with a plus button
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LabelCell" forIndexPath:indexPath];
        
        UILabel* label = (UILabel*)[cell viewWithTag:100];
        [label setText:@"+"];
        //[label setHidden: NO];
        label.textColor = [UIColor blackColor];
        
        cell.backgroundColor = [UIColor clearColor];
        
        [cell.layer setBorderColor: [[UIColor blackColor] CGColor]];
        [cell.layer setBorderWidth: 1.0];
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [self.colorCollection.colors count])
    {
        [self.colorCollection.colors addObject: [UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    }
    else if(editing)
    {
        [self.colorCollection.colors removeObjectAtIndex:indexPath.row];
        
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        return;
    }
    
    if(self.colorCollection.isEditable)
    {
        [self performSegueWithIdentifier:@"colorPickerSegue" sender:nil];
    }
}

-(void)colorSelected:(UIColor *)selectedColor
{
    NSIndexPath *indexPath = [self.collectionView indexPathsForSelectedItems][0];
    
    [self.colorCollection.colors replaceObjectAtIndex:indexPath.row withObject:selectedColor];
    
    [self.collectionView reloadData];
}


- (IBAction)editButtonAction:(id)sender {
    
    editing = !editing;
    
    [self.editButton setTitle:(editing?@"Done":@"Remove")];
    
    if(editing)
    {
        [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.colorCollection.colors count] inSection:0]]];
//        [UIView setAnimationsEnabled:NO];
//        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//        [UIView setAnimationsEnabled:YES];
    }
    else
    {
        [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.colorCollection.colors count] inSection:0]]];
        
//        [UIView setAnimationsEnabled:NO];
//        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//        [UIView setAnimationsEnabled:YES];
    }
    
    NSMutableArray * indexPaths = [NSMutableArray arrayWithCapacity:[self.colorCollection.colors count]];
    
    for(int currCell = 0; currCell < [self.colorCollection.colors count]; ++currCell)
    {
        indexPaths[currCell] = [NSIndexPath indexPathForRow:currCell inSection:0];
    }
    
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"colorPickerSegue"])
    {
        NSIndexPath *indexPath = [self.collectionView indexPathsForSelectedItems][0];
        
        UIColor * color = self.colorCollection.colors[indexPath.row];
        
        ColorPickerViewController * colorPickerViewController = (ColorPickerViewController *)[segue destinationViewController];
        
        colorPickerViewController.color = color;
        colorPickerViewController.delegate = self;
    }
}


@end
