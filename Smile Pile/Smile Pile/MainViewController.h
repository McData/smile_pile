//
//  MainViewController.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/18/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SettingsTableViewController.h"

@interface MainViewController : UIViewController <SettingsTableViewControllerDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) UIPopoverController *currentPopoverController;

-(void) updateDisplayedImage: (UIImage *) newImage;
-(void) pileFinished;

@end
