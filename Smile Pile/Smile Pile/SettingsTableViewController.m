//
//  SettingsTableViewController.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/24/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "ColorPickerViewController.h"
#import "ViewRandomColorColectionsViewController.h"
#import "SettingsData.h"
#import "ColorSettings.h"
#import "Draw.h"

@interface SettingsTableViewController ()

@property (weak, nonatomic) UIImageView * smileyColorUIImageView;
@property (weak, nonatomic) UIImageView * backgroundColorUIImageView;

@property (weak, nonatomic) UILabel * currentColorCollectionNameLabel;

@end

@implementation SettingsTableViewController
{
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    
    if(section == 0){
        return @"Pile Style";
    } else if(section == 1) {
        return @"Max Smiley Size";
    }
    else if (isOverlapping && section == 2) {
        return @"Touch";
    }
    else if(section == 2 + (isOverlapping?1:0)) {
        return @"Colors";
    } else if(section == 3 + (isOverlapping?1:0)) {
        return @"Draw Speed";
    }
    
    return @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    return 5 + (isOverlapping?1:0);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    
    //Pile style
    if(section == 0){
        return 1;
    }
    //Max smiley size
    else if(section == 1) {
        return 1;
    }
    //Touch settings
    else if (isOverlapping && section == 2) {
        return 2;
    }
    //Colors
    else if(section == 2 + (isOverlapping?1:0)) {
        return 3;
    }
    //Draw Speed
    else if(section == 3 + (isOverlapping?1:0)) {
        //Check if pile type is not overlapping
        if(isOverlapping) {
            return 1;
        }
    
        return 2;
    }
    
    else if(section == 4 + (isOverlapping?1:0)) { //Last should always be reset button
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    
    if(indexPath.section == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PileTypeCell"];
        UISegmentedControl * segmentedControl = (UISegmentedControl *)[cell viewWithTag:100];
        
        [self initializeSegmentControl:segmentedControl];
        [segmentedControl addTarget:self
                             action:@selector(PileSettingsChanged:)
                   forControlEvents:UIControlEventValueChanged];
        return  cell;
        
    } else if(indexPath.section == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SliderCell"];
        UISlider * slider = (UISlider *)[cell viewWithTag:100];
        
        [self initializeSmileySizeSlider:slider];
        [slider addTarget:self
                   action:@selector(SmileySizeSliderChanged:)
         forControlEvents:UIControlEventValueChanged];
        
        return cell;
    } else if(indexPath.section == 2 && isOverlapping) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
        UILabel * label = (UILabel *)[cell viewWithTag:100];
        UISwitch * switchView = (UISwitch *)[cell viewWithTag:200];
        
        if(indexPath.row == 0) {
            [label setText:@"Touch to Draw Smiley"];
            switchView.on = [[SettingsData getObjectFromIndex:kDrawSmileOnTouch] boolValue];;
            
            [switchView addTarget:self
                           action:@selector(touchSmileySwitchChanged:)
                 forControlEvents:UIControlEventValueChanged];
        }
        else if (indexPath.row == 1) {
            [label setText:@"Random Sizes"];
            switchView.on = ![[SettingsData getObjectFromIndex:kTouchSmileySetSize] boolValue];;
            
            [switchView addTarget:self
                           action:@selector(touchSmileysSizeSwitchChanged:)
                 forControlEvents:UIControlEventValueChanged];
        }
        return cell;
    
    }else if(indexPath.section == 2 + (isOverlapping?1:0)) {
        
        BOOL randomColors = ((ColorSettings*)[SettingsData getObjectFromIndex:kColorSettings]).useRandomSmileyColors;
        
        //Colors
        if(indexPath.row != 0 && !(indexPath.row == 1 && randomColors)) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ColorCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            UILabel * label = (UILabel *)[cell viewWithTag:100];
            UIImageView * imageView = (UIImageView *)[cell viewWithTag:200];
            
            [imageView.layer setBorderColor: [[UIColor blackColor] CGColor]];
            [imageView.layer setBorderWidth: 1.0];
            
            UIColor * color = [UIColor whiteColor];
            
            if(indexPath.row == 1)
            {
                [label setText:@"Smiley Color"];
                color = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).smileyColor;
                
                self.smileyColorUIImageView = imageView;
            }
            else if (indexPath.row == 2)
            {
                [label setText:@"Background Color"];
                color = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor;
                
                self.backgroundColorUIImageView = imageView;
            }
            
            imageView.image = [Draw imageWithColor: color andBounds: CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
            
            return cell;
        }
        //Random smiley colors
        else if (indexPath.row == 1 && randomColors) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TwoLabelCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            UILabel * label = (UILabel *)[cell viewWithTag:100];
            
            [label setText:@"Colors:"];
            
            label = (UILabel *)[cell viewWithTag:200];
            
            ColorSettings * colorSettings = [SettingsData getObjectFromIndex: kColorSettings];
            
            [label setText: [colorSettings getRandomColorCollectionForIndex:colorSettings.selectedRandomColors].name];
            
            self.currentColorCollectionNameLabel = label;
            
            return cell;
        }
        //Random colors switch
        else if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
            UILabel * label = (UILabel *)[cell viewWithTag:100];
            UISwitch * switchView = (UISwitch *)[cell viewWithTag:200];
            
            [label setText:@"Random Smiley Colors"];
            
            switchView.on = randomColors;
            
            [switchView removeTarget:nil
                               action:NULL
                     forControlEvents:UIControlEventValueChanged];
            
            [switchView addTarget:self
                           action:@selector(randomColorSwitchChanged:)
                 forControlEvents:UIControlEventValueChanged];
            
            return cell;
        }
        
        
    } else if(indexPath.section == 3 + (isOverlapping?1:0)) {
        
        if(indexPath.row == 0)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SliderCell"];
            UISlider * slider = (UISlider *)[cell viewWithTag:100];
            
            [self initializeDrawSpeedSlider:slider];
            [slider addTarget:self
                       action:@selector(DrawSpeedSliderChanged:)
             forControlEvents:UIControlEventValueChanged];
            
            return cell;
        }
        else if(indexPath.row == 1)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
            UILabel * label = (UILabel *)[cell viewWithTag:100];
            UISwitch * switchView = (UISwitch *)[cell viewWithTag:200];
            
            [label setText:@"Continues Pile"];
            
            switchView.on = [((NSNumber*)[SettingsData getObjectFromIndex: kSmilePileLoopBool]) boolValue];
            
            [switchView removeTarget:nil
                              action:NULL
                    forControlEvents:UIControlEventValueChanged];
            
            [switchView addTarget:self
                           action:@selector(ContinuesPileSwitchChanged:)
                 forControlEvents:UIControlEventValueChanged];
            
            
            if([[SettingsData getObjectFromIndex:kSmilePileType] isEqualToString: kOverlappingPileType]) {
                [switchView setEnabled:NO];
            }
            else {
                [switchView setEnabled:YES];
            }
            
            return cell;
        }
    } else if(indexPath.section == 4 + (isOverlapping?1:0)) { //Last item should be reset button
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ButtonCell"];
        UIButton * button = (UIButton *)[cell viewWithTag:100];
        
        [button setTitle:@"Reset Settings" forState:UIControlStateNormal];
        [button addTarget:self
                   action:@selector(ResetButtonAction:)
         forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    
    return nil;
}


-(NSInteger) getIndexForPileSetting: (NSString*) setting
{
    if([setting isEqualToString: kSeparatedPileType])
    {
        return 0;
    }
    else if([setting isEqualToString: kOverlappingPileType])
    {
        return 1;
    }
    else if([setting isEqualToString: kSingleSmileyPileType])
    {
        return 2;
    }

    return -1;
}

-(void) initializeSegmentControl: (UISegmentedControl *) segmentedControl
{
    NSString * selectedPileStyle = [SettingsData getObjectFromIndex:kSmilePileType];
    NSInteger selectedSegment = [self getIndexForPileSetting:selectedPileStyle];
    
    segmentedControl.selectedSegmentIndex = selectedSegment;
}

- (IBAction)PileSettingsChanged:(UISegmentedControl *)sender {
    
    NSString * selectedPileStyle = [SettingsData getObjectFromIndex:kSmilePileType];
    NSInteger prevSelectedSegment = [self getIndexForPileSetting:selectedPileStyle];
    
    switch (sender.selectedSegmentIndex) {
        default:
        case 0:
            [SettingsData setObject:kSeparatedPileType forSetting:kSmilePileType];
            break;
        case 1:
            [SettingsData setObject:kOverlappingPileType forSetting:kSmilePileType];
            break;
        case 2:
            [SettingsData setObject:kSingleSmileyPileType forSetting:kSmilePileType];
            break;
    }
    
    [self.tableView beginUpdates];
    if(sender.selectedSegmentIndex == 1)
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:3]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if(prevSelectedSegment != 0 && prevSelectedSegment != 2)
    {
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:1 inSection:3]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
    [self.tableView endUpdates];
    
}


-(void) initializeSmileySizeSlider: (UISlider *) slider
{
    //Initialize initial smiley size slider
    slider.minimumValue = [(NSNumber *)[SettingsData getObjectFromIndex: kMinSmileySize] floatValue];
    slider.maximumValue = [(NSNumber *)[SettingsData getObjectFromIndex: kMaxSmileySize] floatValue];
    slider.value = [(NSNumber *)[SettingsData getObjectFromIndex: kSmileySize] floatValue];
}

- (IBAction)SmileySizeSliderChanged:(UISlider *)sender {
    
    [SettingsData setObject:[NSNumber numberWithFloat:sender.value] forSetting: kSmileySize];
}

-(void) initializeDrawSpeedSlider: (UISlider *) slider
{
    float minDrawSpeed = [(NSNumber *)[SettingsData getObjectFromIndex: kSmileyMinDrawSpeed] floatValue];
    float maxDrawSpeed = [(NSNumber *)[SettingsData getObjectFromIndex: kSmileyMaxDrawSpeed] floatValue];
    float currDrawSpeed = [(NSNumber* ) [SettingsData getObjectFromIndex:kSmileyDrawSpeed] floatValue];
    
    slider.minimumValue = 0;
    slider.maximumValue = maxDrawSpeed - minDrawSpeed;
    slider.value = maxDrawSpeed - currDrawSpeed;
}

- (IBAction)DrawSpeedSliderChanged:(UISlider *)sender {
    
    float maxDrawSpeed = [(NSNumber *)[SettingsData getObjectFromIndex: kSmileyMaxDrawSpeed] floatValue];
    
    float newDrawSpeed = maxDrawSpeed - sender.value;
    
    [SettingsData setObject: [NSNumber numberWithFloat: newDrawSpeed] forSetting: kSmileyDrawSpeed];
}

- (IBAction)ContinuesPileSwitchChanged:(UISwitch *)sender {
    [SettingsData setObject:[NSNumber numberWithBool:sender.on] forSetting:kSmilePileLoopBool];
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL randomColors = ((ColorSettings*)[SettingsData getObjectFromIndex:kColorSettings]).useRandomSmileyColors;
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    
    if(indexPath.section == 2 + (isOverlapping?1:0) && indexPath.row != 0)
    {
        if (!(indexPath.row == 1 && randomColors)) {
            [self performSegueWithIdentifier:@"colorPickerSegue" sender:nil];
        }
        else {
            [self performSegueWithIdentifier:@"viewRandomColorCollectionsSegue" sender:nil];
        }
    }
    
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"colorPickerSegue"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        UIColor * color;
        
        if(indexPath.row == 1)
        {
            color = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).smileyColor;
        }
        else if (indexPath.row == 2)
        {
            color = ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor;
        }
        
        ColorPickerViewController * colorPickerViewController = (ColorPickerViewController *)[segue destinationViewController];
        
        colorPickerViewController.color = color;
        colorPickerViewController.delegate = self;
    }
    else if ([[segue identifier] isEqualToString:@"viewRandomColorCollectionsSegue"])
    {
        ViewRandomColorColectionsViewController * controller = (ViewRandomColorColectionsViewController*) [segue destinationViewController];
        controller.currentColorCollectionNameLabel = self.currentColorCollectionNameLabel;
    }
}


-(void) colorSelected:(UIColor *)selectedColor {
    [self updateColorSettings:selectedColor];
}

-(void) updateColorSettings: (UIColor *) newColor
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    UIImageView * imageView;
    
    if(indexPath.row == 1)
    {
        ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).smileyColor = newColor;
        
        imageView = self.smileyColorUIImageView;
    }
    else if (indexPath.row == 2)
    {
        ((ColorSettings * )[SettingsData getObjectFromIndex: kColorSettings]).backgroundColor = newColor;
        
        imageView = self.backgroundColorUIImageView;
    }
    
    imageView.image = [Draw imageWithColor: newColor andBounds: CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
}


- (IBAction)ResetButtonAction:(UIButton *)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Settings"
                                                    message:@"Are you sure you want to reset all settings back to their default values?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes",nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [SettingsData revertToDefault];
        
        
        //[self.tableView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,[self numberOfSectionsInTableView:self.tableView])] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView reloadData];
        [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
}

-(IBAction)randomColorSwitchChanged:(UISwitch *)sender
{
    BOOL isOverlapping = [[SettingsData getObjectFromIndex: kSmilePileType] isEqualToString: kOverlappingPileType];
    
    ((ColorSettings*)[SettingsData getObjectFromIndex:kColorSettings]).useRandomSmileyColors = sender.on;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:2 + (isOverlapping?1:0)]] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2 + (isOverlapping?1:0)] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(IBAction)touchSmileySwitchChanged:(UISwitch *)sender
{
    [SettingsData setObject: [NSNumber numberWithBool: sender.on] forSetting: kDrawSmileOnTouch];
}

-(IBAction)touchSmileysSizeSwitchChanged:(UISwitch *)sender
{
    [SettingsData setObject: [NSNumber numberWithBool: !sender.on] forSetting: kTouchSmileySetSize];
}



-(void) viewWillDisappear:(BOOL)animated
{
    [SettingsData saveSettings];
}

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate settingsTableViewControllerDidFinish:self];
}

@end
