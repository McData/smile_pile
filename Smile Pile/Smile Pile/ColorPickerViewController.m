//
//  ColorPickerViewController.m
//  Smile Pile
//
//  Created by Joshua Linge on 7/26/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "ColorPickerViewController.h"
#import "SettingsData.h"
#import "Draw.h"
#import "ColorSettings.h"

@interface ColorPickerViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *CurrentColorImageView;

@property (weak, nonatomic) IBOutlet UIButton *ColorPreset1Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset2Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset3Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset4Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset5Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset6Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset7Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorPreset8Button;


@property (weak, nonatomic) IBOutlet UIButton *ColorHistory1Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory2Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory3Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory4Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory5Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory6Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory7Button;
@property (weak, nonatomic) IBOutlet UIButton *ColorHistory8Button;


@property (weak, nonatomic) IBOutlet UIImageView *CustomColorImageView;
@property (weak, nonatomic) IBOutlet UILabel *RedLabel;
@property (weak, nonatomic) IBOutlet UILabel *GreenLabel;
@property (weak, nonatomic) IBOutlet UILabel *BlueLabel;
@property (weak, nonatomic) IBOutlet UISlider *RedSlider;
@property (weak, nonatomic) IBOutlet UISlider *GreenSlider;
@property (weak, nonatomic) IBOutlet UISlider *BlueSlider;


@end

@implementation ColorPickerViewController
{
    UIColor * customColor;
    ColorSettings * colorSettings;
}

static CGRect imageFrame;
static BOOL initialized;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(!initialized)
    {
        initialized = YES;
        
        
        imageFrame = CGRectMake(0, 0, self.CurrentColorImageView.frame.size.width, self.CurrentColorImageView.frame.size.height);
    }

    colorSettings = [SettingsData getObjectFromIndex:kColorSettings];
    
    [self.CurrentColorImageView.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.CurrentColorImageView.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset1Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:0] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset1Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset1Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset2Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:1] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset2Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset2Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset3Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:2] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset3Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset3Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset4Button setImage:[Draw imageWithColor:[colorSettings getColorPresetForIndex:3] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset4Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset4Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset5Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:4] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset5Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset5Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset6Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:5] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset6Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset6Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset7Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:6] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset7Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset7Button.layer setBorderWidth: 1.0];
    
    
    [self.ColorPreset8Button setImage:[Draw imageWithColor: [colorSettings getColorPresetForIndex:7] andBounds:imageFrame] forState:UIControlStateNormal];
    [self.ColorPreset8Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorPreset8Button.layer setBorderWidth: 1.0];
        
        
        
        
    [self.ColorHistory1Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory1Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory2Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory2Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory3Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory3Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory4Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory4Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory5Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory5Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory6Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory6Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory7Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory7Button.layer setBorderWidth: 1.0];
    
    [self.ColorHistory8Button.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.ColorHistory8Button.layer setBorderWidth: 1.0];
    
    
    
    [self.CustomColorImageView.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [self.CustomColorImageView.layer setBorderWidth: 1.0];
    
//    [self.RedSlider setThumbTintColor: [UIColor redColor]];
//    [self.GreenSlider setThumbTintColor: [UIColor greenColor]];
//    [self.BlueSlider setThumbTintColor: [UIColor blueColor]];
    
    self.CurrentColorImageView.image = [Draw imageWithColor: self.color andBounds:imageFrame];
    
    [self updateCustomColorArea: self.color];
    [self updateHistoryDisplay];
}


-(void) updateCustomColorArea: (UIColor *) newColor
{
    self.CustomColorImageView.image = [Draw imageWithColor: newColor andBounds:imageFrame];
    
    CGFloat colorComponents[4];
    [newColor getRed:&colorComponents[0] green:&colorComponents[1] blue:&colorComponents[2] alpha:&colorComponents[3]];
    
    if(newColor != customColor)
    {
        self.RedSlider.value = colorComponents[0];
        self.GreenSlider.value = colorComponents[1];
        self.BlueSlider.value = colorComponents[2];
    }
    
    customColor = newColor;
    
    @autoreleasepool {
        
        CGSize size = CGSizeMake(256, 20);
        
        size.width = roundf(colorComponents[0] * 256);
        UIImage * gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor:[UIColor colorWithRed:0.0 green:colorComponents[1] blue:colorComponents[2] alpha:colorComponents[3]] andEndColor:customColor];
        [self.RedSlider setMinimumTrackImage: gradientImage forState:UIControlStateNormal];
        
        
        size.width = roundf(256 - (colorComponents[0] * 256));
        gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor:customColor andEndColor:[UIColor colorWithRed:1.0 green:colorComponents[1] blue:colorComponents[2] alpha:colorComponents[3]]];
        [self.RedSlider setMaximumTrackImage: gradientImage forState:UIControlStateNormal];
        
        
        
        size.width = roundf(colorComponents[1] * 256);
        gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor:[UIColor colorWithRed:colorComponents[0] green:0.0 blue:colorComponents[2] alpha:colorComponents[3]] andEndColor:customColor];
        [self.GreenSlider setMinimumTrackImage: gradientImage forState:UIControlStateNormal];
        
        
        size.width = roundf(256 - (colorComponents[1] * 256));
        gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor:customColor andEndColor:[UIColor colorWithRed:colorComponents[0] green:1.0 blue:colorComponents[2] alpha:colorComponents[3]]];
        [self.GreenSlider setMaximumTrackImage: gradientImage forState:UIControlStateNormal];
        
        
        
        size.width = roundf(colorComponents[2] * 256);
        gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor: [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:0.0 alpha:colorComponents[3]] andEndColor:customColor];
        [self.BlueSlider setMinimumTrackImage: gradientImage forState:UIControlStateNormal];
        
        
        size.width = roundf(256 - (colorComponents[2] * 256));
        gradientImage = [self getColorSliderGradientImageWithSize:size andStartColor:customColor andEndColor: [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:1.0 alpha:colorComponents[3]]];
        [self.BlueSlider setMaximumTrackImage: gradientImage forState:UIControlStateNormal];
    }
    
    [self.RedLabel setText:[NSString stringWithFormat:@"%iR", (int)(colorComponents[0]*255)]];
    [self.GreenLabel setText:[NSString stringWithFormat:@"%iG", (int)(colorComponents[1]*255)]];
    [self.BlueLabel setText:[NSString stringWithFormat:@"%iB", (int)(colorComponents[2]*255)]];
}

-(UIImage *) getColorSliderGradientImageWithSize: (CGSize) size andStartColor: (UIColor *) startColor andEndColor: (UIColor *) endColor
{
    if(size.width == 0)
        return [Draw imageWithColor:endColor andBounds:CGRectMake(0, 0, 1, size.height)];
    
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [Draw drawGradientLinearOnContext:context from:CGPointMake(0, 0) to:CGPointMake(size.width, 0) withStartColor:startColor.CGColor andEndColor:endColor.CGColor];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

-(void) updateCurrentColor: (UIColor*) newColor
{
    self.color = newColor;
    self.CurrentColorImageView.image = [Draw imageWithColor: self.color andBounds:imageFrame];
    
    [self updateCustomColorArea: self.color];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.delegate colorSelected: self.color];
    
    [self addToHistoryArray:self.color];
}

- (IBAction)ColorPresetSelected:(UIButton*)sender {
    
    NSInteger tag = sender.tag;
    
    [self updateCurrentColor:[colorSettings getColorPresetForIndex:tag]];
}



- (IBAction)ColorHistorySelected:(UIButton*)sender {
    
    NSInteger tag = sender.tag;
    
    if(tag >= [colorSettings ColorHistoryCount])
        return;
    
    [self updateCurrentColor:[colorSettings getColorHistoryForIndex:tag]];
}



-(void) addToHistoryArray: (UIColor *) newColor
{
    [colorSettings addColorToColorHistory:newColor];
    
    [self updateHistoryDisplay];
}

-(void) updateHistoryDisplay
{
    NSInteger count = [colorSettings ColorHistoryCount];
    
    if(count == 0)
        return;
    
    [self.ColorHistory1Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:0] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 1)
        return;
    
    [self.ColorHistory2Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:1] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 2)
        return;
    
    [self.ColorHistory3Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:2] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 3)
        return;
    
    [self.ColorHistory4Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:3] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 4)
        return;
    
    [self.ColorHistory5Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:4] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 5)
        return;
    
    [self.ColorHistory6Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:5] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 6)
        return;
    
    [self.ColorHistory7Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:6] andBounds:imageFrame] forState:UIControlStateNormal];
    
    if(count == 7)
        return;
    
    [self.ColorHistory8Button setImage:[Draw imageWithColor: [colorSettings getColorHistoryForIndex:7] andBounds:imageFrame] forState:UIControlStateNormal];
    
}


- (IBAction)colorSliderChanged:(UISlider *)sender {
    
    CGFloat colorComponents[4];
    [customColor getRed:&colorComponents[0] green:&colorComponents[1] blue:&colorComponents[2] alpha:&colorComponents[3]];
    
    colorComponents[sender.tag] = sender.value;
    
    customColor = [UIColor colorWithRed:colorComponents[0] green:colorComponents[1] blue:colorComponents[2] alpha:colorComponents[3]];
    
//    [self updateCustomColorArea: customColor];
    [self updateCurrentColor: customColor];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

}
*/

@end
