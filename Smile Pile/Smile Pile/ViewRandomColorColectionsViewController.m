//
//  ViewRandomColorColectionsViewController.m
//  Smile Pile
//
//  Created by Joshua Linge on 8/11/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import "ViewRandomColorColectionsViewController.h"
#import "ColorSettings.h"
#import "SettingsData.h"
#import "ViewRandomColorsViewController.h"

@interface ViewRandomColorColectionsViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@end

@implementation ViewRandomColorColectionsViewController
{
    ColorSettings * colorSettings;
    BOOL editing;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 480.0);
    [super awakeFromNib];
}

- (void)viewWillAppear:(BOOL)animated
{
    colorSettings = [SettingsData getObjectFromIndex:kColorSettings];
    [self.tableView setEditing: YES animated: YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

- (IBAction)editButtonAction:(id)sender {
    
    editing = !editing;
    
    [self.editButton setTitle:(editing?@"Done":@"Edit")];
    
    //[self.tableView setEditing: editing animated: YES];
    [self.tableView setEditing: NO animated: YES];
    [self.tableView setEditing: YES animated: YES];
    
    
    [self.tableView beginUpdates];
    if(editing)
    {
        for (UITableViewCell* cell in [self.tableView visibleCells])
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: [colorSettings RandomColorCount] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        
        
        //[self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: [colorSettings RandomColorCount] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: [colorSettings RandomColorCount] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        
        for (int currCell = 0; currCell < [self.tableView visibleCells].count; ++currCell)
        {
            UITableViewCell* cell = [self.tableView visibleCells][currCell];
            
            if(currCell == colorSettings.selectedRandomColors)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        
        //[self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        //[self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: [colorSettings RandomColorCount] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    [self.tableView endUpdates];
    
    
    //[self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [colorSettings RandomColorCount] + (!editing?1:0);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel * label = (UILabel*) [cell viewWithTag:100];
    
    if(indexPath.row < [colorSettings RandomColorCount])
    {
        [label setText:[colorSettings getRandomColorCollectionForIndex: indexPath.row].name];
        cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else
    {
        [label setText:@"New set"];
        cell.editingAccessoryType = UITableViewCellAccessoryNone;
    }
    
    if(!editing)
    {
        if(indexPath.row == colorSettings.selectedRandomColors)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [colorSettings RandomColorCount])
    {
        return UITableViewCellEditingStyleInsert;
    }
    
    if(editing && [self tableView:tableView canEditRowAtIndexPath:indexPath])
    {
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        return UITableViewCellEditingStyleNone;
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [colorSettings RandomColorCount])
        return YES;
    
    // Return NO if you do not want the specified item to be editable.
    return editing && [colorSettings getRandomColorCollectionForIndex: indexPath.row].isEditable;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        if(indexPath.row == colorSettings.selectedRandomColors)
        {
            [self selectColorCollection:0];
        }
        
        [colorSettings removeColorCollectionAtIndex:indexPath.row];
        
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        [self newSetAction];
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!editing)
    {
        if(indexPath.row != [colorSettings RandomColorCount])
        {
            [self selectColorCollection:indexPath.row];
            [self.tableView reloadData];
        }
        else
        {
            [self newSetAction];
        }
    }
    else
    {
        if(indexPath.row != [colorSettings RandomColorCount])
        {
            [self performSegueWithIdentifier:@"viewRandomColorsSegue" sender:nil];
        }
//        else
//        {
//            //[tableView deselectRowAtIndexPath:indexPath animated:YES];
//            [self newSetAction];
//        }
    }
}

-(void) newSetAction
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"New Color Set" message:@"Pick a name for the new color set." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString * name = [[alertView textFieldAtIndex:0] text];
    if (buttonIndex == 1 && [name length] > 0) {
        
        [colorSettings createColorCollectionWithName: name];
        
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[colorSettings RandomColorCount] inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        UITableViewCell * cell = [self.tableView visibleCells][[colorSettings RandomColorCount]-1];
        
        [ ((UILabel*)[cell viewWithTag:100]) setText:[colorSettings getRandomColorCollectionForIndex:[colorSettings RandomColorCount]-1].name];
        cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        [self performSegueWithIdentifier:@"viewRandomColorsSegue" sender:nil];
    }
    else{
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        if(indexPath != nil) {
            [self.tableView deselectRowAtIndexPath: indexPath animated:YES];
        }
    }
}

-(void) selectColorCollection: (NSInteger) index
{
    ((UITableViewCell *)[self.tableView visibleCells][colorSettings.selectedRandomColors]).accessoryType = UITableViewCellAccessoryNone;
    ((UITableViewCell *)[self.tableView visibleCells][index]).accessoryType = UITableViewCellAccessoryCheckmark;
    
    colorSettings.selectedRandomColors = index;
    [self.currentColorCollectionNameLabel setText: [colorSettings getRandomColorCollectionForIndex:colorSettings.selectedRandomColors].name];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"viewRandomColorsSegue"])
    {
        ViewRandomColorsViewController * controller = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        controller.colorCollection = [colorSettings getRandomColorCollectionForIndex:indexPath.row];
        
        [self selectColorCollection:indexPath.row];
        
        if(editing)
        {
            [self editButtonAction:nil];
        }
        else
        {
            [self.tableView setEditing: NO animated: YES];
            [self.tableView setEditing: YES animated: YES];
        }
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
