//
//  SettingsTableViewController.h
//  Smile Pile
//
//  Created by Joshua Linge on 7/24/14.
//  Copyright (c) 2014 BlueDevGroup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerViewController.h"

@class SettingsTableViewController;

@protocol SettingsTableViewControllerDelegate
- (void)settingsTableViewControllerDidFinish:(SettingsTableViewController *)controller;
@end

@interface SettingsTableViewController : UITableViewController <ColorPickerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) id <SettingsTableViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;
-(void) updateColorSettings: (UIColor *) newColor;

@end
